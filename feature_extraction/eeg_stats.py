import numpy as np

import mne
from mne.time_frequency import psd_welch
from mne.connectivity import spectral_connectivity
mne.set_log_level(verbose='ERROR')

from .eeg_microstates import *

_FREQ_BANDS = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha": [8, 13],
               "beta": [13, 25],
               "gamma": [25, 40]}

def eeg_power_band(epochs, relative=True, freq_bands=_FREQ_BANDS, freq_range = (1.0, 45.), spectrum=False):
    """EEG relative power band feature extraction.

    This function takes an ``mne.Epochs`` object and creates EEG features based
    on relative power in specific frequency bands that are compatible with
    scikit-learn.

    Parameters
    ----------
    epochs : Epochs
        The data.
    relative : Whether returned values are relative
    freq_bands: frequency bands to calculate PSD features

    Returns
    -------
    tot_power : total power per channel
    X : numpy array of shape [n_samples, 5]
        Transformed data.
    """

    psds, freqs = psd_welch(epochs, fmin=freq_range[0], fmax=freq_range[1])
    tot_power = np.sum(psds, axis=-1, keepdims=True)
    if relative:
        # Normalize the PSDs
        psds /= tot_power

    X = []
    for fmin, fmax in freq_bands.values():
        psds_band = psds[:, :, (freqs >= fmin) & (freqs < fmax)].sum(axis=-1)
        X.append(psds_band.reshape(len(psds), -1))

    if spectrum:
        return np.squeeze(tot_power, axis=-1), np.array(X), psds

    return np.squeeze(tot_power, axis=-1), np.array(X)


def generate_topomap_data(pib, info, resolution=32):
    """
    Generates topomap for a given set of power in band values.
    Args:
        pib: relative power in band values
        info: MNE info structure of EEG data
        resolution: resolution of resulting topomap (square)

    Returns:

    """

    topo_data = []
    for i in range(pib.shape[0]):
        a = mne.viz.plot_topomap(pib[i, :], info, sensors=False, contours=0, cmap='gray', show=False,
                                 outlines=None, res=resolution, vmin=0, vmax=1)
        topo_data.append(a[0].get_array())

    return np.array(topo_data)


def calc_connectivity(epochs, method='imcoh', average=True, freq_bands=_FREQ_BANDS,
                      low_freq_range=None, high_freq_range=None):
    """EEG spectral connectivity.

        This function takes an ``mne.Epochs`` object and creates EEG features based
        on spectral connectivity.

        Parameters
        ----------
        epochs : Epochs
            The data.

        method : Connectivity measure to be used (default = "coh")
        average : whether to return average connectivity across all epochs
        freq_bands: frequency bands to calculate PSD features

        Returns
        -------
        con : connectivity matrix.
        """

    if (not low_freq_range) and (not high_freq_range):
        low_fr_range = [freq_bands[band][0] for band in freq_bands]
        high_fr_range = [freq_bands[band][1] for band in freq_bands]
    else:
        low_fr_range = low_freq_range
        high_fr_range = high_freq_range

    if average:
        con, freqs, times, n_epochs, n_tapers = spectral_connectivity(epochs, method=method, mode='multitaper',
                                                                      sfreq=epochs.info['sfreq'], fmin=low_fr_range,
                                                                      fmax=high_fr_range, faverage=True, tmin=0,
                                                                      mt_adaptive=False, n_jobs=1)
    else:
        con = []
        for ep in epochs:
            ep_con, _, _, _, _ = spectral_connectivity(np.expand_dims(ep, axis=0), method=method, mode='multitaper',
                                                                          sfreq=epochs.info['sfreq'], fmin=low_fr_range,
                                                                          fmax=high_fr_range, faverage=True, tmin=0,
                                                                          mt_adaptive=False, n_jobs=1)
            con.append(ep_con)

    return np.array(con)


def count_eye_blinks(wake_epochs, threshold='auto'):
    '''
    Compute the number of eye blinks in each epoch.
    Recommended:
        1. Pass only wake epochs
        2. Do not perform artifact removal prior to this.
        Remove bad epochs if possible (flat-line channel, etc.)
    Args:
        wake_epochs: MNE epoch structure containing wake data only.
        threshold: threshold to detect eye blinks (pass 3e-4 for epochs that may not contain blinks, otherwise 'auto')

    Returns:
        blink_counts: # blinks in each epoch
    '''

    combined_data = mne.concatenate_epochs([wake_epochs[i] for i in range(len(wake_epochs))], add_offset=False)
    split_data = np.vsplit(combined_data.get_data(), len(combined_data))
    full_data = np.squeeze(np.dstack(split_data), axis=0)
    full_raw = mne.io.RawArray(full_data, wake_epochs.info)

    eye_channels = [ch for ch in full_raw.ch_names if 'fp' in ch.lower()]
    used_eye_channel = 'fpz' if 'fpz' in eye_channels else eye_channels[0]

    eog_signal = np.reshape(full_raw.copy().pick(picks=used_eye_channel).get_data(), [-1])
    if threshold == 'auto':
        thresh = min(3e-4, np.abs(np.max(eog_signal) - np.min(eog_signal)) / 8)
    else:
        thresh = threshold

    filter_length = mne.filter.next_fast_len(int(round(4 * full_raw.info['sfreq'])))

    blink_counts = []
    for i in range(len(wake_epochs)):

        epoch = wake_epochs[i]

        epoch_data = np.squeeze(epoch.get_data(), axis=0)
        epoch_raw = mne.io.RawArray(epoch_data, wake_epochs.info)

        eog_events = mne.preprocessing.find_eog_events(epoch_raw, ch_name=used_eye_channel, h_freq=5.,
                                                       thresh=thresh, filter_length=filter_length)
        blink_counts.append(len(eog_events))

    return np.array(blink_counts), thresh


def calc_microstate_stats(data4ms, fs, ch_names, n_maps=4):
    mode = ["aahc", "kmeans", "kmedoids", "pca", "ica"][1]

    if (len(ch_names) != data4ms.shape[1]):
        data4ms = data4ms.T
    locs = []
    maps, state_seq, gfp, gfp_peaks, gev = clustering(data4ms, fs, ch_names, locs, mode, n_maps, doplot=False)

    gfp_per_state = np.arange(4, dtype=float)

    for st in range(n_maps):
        gfp_per_state[st] = np.mean(gfp[state_seq==st])

    p_hat = p_empirical(state_seq, n_maps)
    T_hat = T_empirical(state_seq, n_maps)

    return gfp, maps, gfp_per_state, p_hat, T_hat, gev, gev.sum()