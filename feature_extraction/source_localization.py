import os.path as op

import nibabel
import numpy as np

from nilearn.datasets import load_mni152_template
from nilearn.image import resample_to_img

import mne
mne.set_log_level(verbose='ERROR')
from mne.datasets import fetch_fsaverage
from mne.filter import next_fast_len
from mne.time_frequency import psd_welch

from joblib import Parallel, delayed

_FREQ_BANDS = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25],
               "gamma": [25, 40]}

_VOLUME_LABELS = ['Left-Cerebral-Cortex', 'Right-Cerebral-Cortex']


class SourceLocalization(object):
    '''Class that defines EEG source localization using FS-average volume.'''

    def __init__(self, freq_bands=_FREQ_BANDS, epoch_len=10., vol_source_pos=5.0, vol_labels=None):
        '''
        Initialize directories and templates for source localization.
        '''
        self._freq_bands = freq_bands
        self._epoch_len = epoch_len

        # Download fsaverage files
        self._fs_dir = fetch_fsaverage(verbose='WARNING')
        self._fs_subjects_dir = op.dirname(self._fs_dir)

        # The files live in:
        self._subject = 'fsaverage'
        self._trans = 'fsaverage'  # MNE has a built-in fsaverage transformation
        # self._surf_src = op.join(self._fs_dir, 'bem', 'fsaverage-ico-5-src.fif')
        self._surf_src = mne.setup_source_space(self._subject, spacing='ico5',
                                                add_dist=False, subjects_dir=self._fs_subjects_dir)
        self._bem = op.join(self._fs_dir, 'bem', 'fsaverage-5120-5120-5120-bem-sol.fif')
        self._mri = op.join(self._fs_dir, 'mri', 'T1.mgz')
        self._mni152_mri = load_mni152_template()
        self._nifti_header = nibabel.load(self._mri).header
        self._fname_aseg = op.join(self._fs_subjects_dir, self._subject, 'mri', 'aseg.mgz')
        self._volume_labels = vol_labels
        if not self._volume_labels:
            # self._vol_src = op.join(self._fs_dir, 'bem', 'fsaverage-vol-5-src.fif')
            self._vol_src = mne.setup_volume_source_space(self._subject, mri=self._mri, pos=vol_source_pos,
                                                          bem=self._bem, subjects_dir=self._fs_subjects_dir,
                                                          add_interpolator=True, verbose=False)
        else:
            self._vol_src = mne.setup_volume_source_space(self._subject, mri=self._fname_aseg, pos=vol_source_pos,
                                                          bem=self._bem, subjects_dir=self._fs_subjects_dir,
                                                          volume_label=self._volume_labels, add_interpolator=True,
                                                          verbose=False)

        # self._sample_dir = mne.datasets.sample.data_path()
        # self._sample_subjects_dir = op.join(self._sample_dir, 'subjects')

        self._volumetric = True
        self._fwd = None
        self._inv = None
        self._src_method = 'dSPM'


    def reset(self):
        '''
        Reset parameters.
        :return:
        '''

        self._volumetric = True
        self._fwd = None
        self._inv = None
        self._src_method = 'dSPM'

    def _compute_forward(self, epochs, volume=True, n_jobs=1):
        '''
        Compute forward operator.
        Args:
            epochs:
            volume:
            n_jobs:

        Returns:

        '''
        if epochs is None:
            return None

        if volume:
            self._fwd = mne.make_forward_solution(epochs.info, trans=self._trans, src=self._vol_src,
                                            bem=self._bem, eeg=True, meg=False, mindist=5.0, n_jobs=n_jobs)
        else:
            self._fwd = mne.make_forward_solution(epochs.info, trans=self._trans, src=self._surf_src,
                                            bem=self._bem, eeg=True, meg=False, mindist=5.0, n_jobs=n_jobs)

        self._volumetric = volume

    def _compute_inverse_sol(self, epochs):
        '''
        Compute the inverse solution for the epochs structure.
        '''

        if epochs is None:
            return None

        cov = mne.compute_covariance(epochs, tmax=self._epoch_len)

        # if len(epochs) > 1:
        #     cov = mne.compute_covariance(epochs, tmax=self._epoch_len)
        # else:
        #     raw = mne.io.RawArray(np.squeeze(epochs[0].get_data(), axis=0), epochs.info)
        #     cov = mne.compute_raw_covariance(raw, tmax=self._epoch_len)

        self._inv = mne.minimum_norm.make_inverse_operator(epochs.info, self._fwd, cov, verbose=False)

        del cov

    def _compute_source_estimate_epochs(self, epochs):
        '''
        Compute source for epochs object (in spectral domain).
        '''

        snr = 3.
        lambda2 = 1. / snr ** 2

        # n_fft = next_fast_len(int(round(4 * epochs.info['sfreq'])))
        fmin, fmax = .5, 45.5
        bandwidth = 1.

        stcs = mne.minimum_norm.compute_source_psd_epochs(epochs, self._inv, lambda2=lambda2, method=self._src_method,
                                                          fmin=fmin, fmax=fmax, bandwidth=bandwidth,
                                                          return_generator=False, verbose=False)

        return stcs

    def _compute_source_estimate_raw(self, raw, n_jobs=1):
        '''
        Compute source estimates for MNE raw objects using the specified source and method (in spectral domain).
        '''

        snr = 3.
        lambda2 = 1. / snr ** 2

        fmin, fmax = 1., 45.
        bandwidth = 1.

        n_fft = next_fast_len(int(round(4 * raw.info['sfreq'])))

        stc = mne.minimum_norm.compute_source_psd(raw, self._inv, lambda2=lambda2, method=self._src_method,
                                                  n_fft=n_fft, fmin=fmin, fmax=fmax, bandwidth=bandwidth,
                                                  dB=False, return_sensor=False, verbose=False, n_jobs=n_jobs)

        return stc

    def _compute_source_estimate_raw_time(self, raw):
        '''
        Compute source estimates for MNE raw objects using the specified source and method (in time domain).
        '''

        snr = 3.
        lambda2 = 1. / snr ** 2

        stc = mne.minimum_norm.apply_inverse_raw(raw, self._inv, lambda2=lambda2)

        return stc

    def compute_source_estimate_epochs(self, epochs, volume=True, method='dSPM', avg=True, relative=True,
                                       full_spectrum=False, n_jobs=1):
        '''
        Compute source estimates for MNE epochs objects using the specified source and method.
        '''

        if epochs is None:
            return None

        if self._volumetric != volume:
            self._compute_forward(epochs, volume=volume, n_jobs=n_jobs)

        if self._fwd is None:
            self._compute_forward(epochs, volume=volume, n_jobs=n_jobs)

        self._compute_inverse_sol(epochs)

        self._src_method = method

        stcs = self._compute_source_estimate_epochs(epochs)
        # stcs_ret = Parallel(n_jobs=n_jobs)(delayed(self._compute_source_estimate_epochs)(epochs[i])
        #                                    for i in range(len(epochs)))
        #
        # stcs = [stc[0] for stc in stcs_ret]

        stc_bands = dict()

        if avg:
            n_epochs = len(epochs)
            psd_avg = 0.
            for i, stc in enumerate(stcs):
                psd_avg += np.sum(stc.data.reshape(stc.data.shape[0], stc.data.shape[1]//10, 10), axis=-1)
            psd_avg /= n_epochs

            stc.data = psd_avg

            del psd_avg, stcs

            if full_spectrum:
                return stc

            stc_norm = stc.sum()
            if not relative:
                stc_norm.data /= stc_norm.data

            for band, limits in self._freq_bands.items():
                stc_bands[band] = stc.copy().crop(*limits).sum() / stc_norm.data

            del stc
            # psd_avg = 0.
            # for i, stc in enumerate(stcs):
            #     psd_avg += np.log10(100000 * stc.data)
            # psd_avg /= n_epochs
            # stc.data = psd_avg
            #
            # for band, limits in self._freq_bands.items():
            #     stc_bands[band] = stc.copy().crop(*limits).sum()
        else:
            if full_spectrum:
                return stcs

            for i, stc in enumerate(stcs):
                stc_norm = stc.sum()
                if not relative:
                    stc_norm.data /= stc_norm.data
                for band, limits in self._freq_bands.items():
                    stc_bands[band].append(stc.copy().crop(*limits).sum() / stc_norm.data)

            del stcs, stc, stc_norm
        #         stcs_ret = Parallel(n_jobs=n_jobs)(delayed(self._compute_source_estimate_single_epoch)(epochs[i])
        #                                        for i in range(len(epochs)))

        #         stcs = [stc[0] for stc in stcs_ret]

        #         stc = None
        #         if avg:
        #             # compute average PSD over all epochs
        #             psd_avg = 0.
        #             for i, stc in enumerate(stcs):
        #                 psd_avg += stc.data
        #             psd_avg /= len(ec_epochs)
        #             freqs = stc.times  # the frequencies are stored here
        #             stc.data = psd_avg  # overwrite the last epoch's data with the average

        # stc_bands = dict()
        #
        # psds, freqs = psd_welch(epochs, fmin=1.0, fmax=45., picks='eeg')
        #
        # if avg:
        #     combined_data = mne.concatenate_epochs([epochs[i] for i in range(len(epochs))], add_offset=False)
        #     split_data = np.vsplit(combined_data.get_data(), len(combined_data))
        #     full_data = np.squeeze(np.dstack(split_data), axis=0)
        #     full_raw = mne.io.RawArray(full_data, epochs.info)
        #
        #     stc = self._compute_source_estimate_raw(full_raw)
        #
        #     # psds = np.mean(psds, axis=0)
        #     # raw = mne.io.RawArray(psds, epochs.pick(picks='eeg').info)
        #     # raw.info.update({'sfreq': 1.})
        #     #
        #     # stc = self._compute_source_estimate_raw_time(raw)
        #
        #     stc_norm = stc.sum()
        #     for band, limits in self._freq_bands.items():
        #         stc_bands[band] = 100 * stc.copy().crop(*limits).sum() / stc_norm.data
        # else:
        #     # stcs_ret = Parallel(n_jobs=n_jobs)(delayed(self._compute_source_estimate_epochs)(epochs[i])
        #     #                            for i in range(len(epochs)))
        #     #
        #     # stcs = [stc[0] for stc in stcs_ret]
        #     stcs = []
        #
        #     for i in range(len(epochs)):
        #         raw = mne.io.RawArray(psds[i, :, :], epochs.pick(picks='eeg').info)
        #         raw.info.update({'sfreq': 1.})
        #
        #         stc = self._compute_source_estimate_raw_time(raw)
        #         stcs.append(stc)
        #
        #     for i, stc in enumerate(stcs):
        #         stc_norm = stc.sum()
        #         for band, limits in self._freq_bands.items():
        #             stc_bands[band] = []
        #             stc_bands[band].append(100 * stc.copy().crop(*limits).sum() / stc_norm.data)

        return stc_bands

    def do_plot(self, stc):
        '''
        Plot source estimate.
        '''

        if stc is None:
            return

        if self._volumetric:
            fig = stc.plot(self._vol_src, subject='fsaverage', subjects_dir=self._fs_subjects_dir, mode='glass_brain')
        #             fig = stc.plot_3d(src=self._vol_src, subjects_dir=self._fs_subjects_dir,
        #                               hemi='lh', view_layout='horizontal', views='med',
        #                               time_viewer=False, backend='matplotlib',volume_options=dict(resolution=1.),
        #                               add_data_kwargs=dict(colorbar_kwargs=dict(label_font_size=8)))
        else:
            fig = stc.plot(views='dor', hemi='rh', colormap='inferno', time_viewer=False, show_traces=False,
                           backend='matplotlib', clim=dict(kind='percent', lims=(70, 85, 99)), smoothing_steps=10)

        return fig

    def export_nifti_image(self, stc, nii_name, resample_to_mni152=True, return_image=False):
        '''
        Export volume source estimate as a Nifti image.
        '''

        if not self._volumetric:
            return

        if not nii_name.endswith('.nii.gz'):
            nii_name = nii_name + '.nii.gz'

        # stc.save_as_volume(nii_name, self._vol_src, mri_resolution=True, format='nifti1')

        img = stc.as_volume(self._vol_src, mri_resolution=True)  # set True for full MRI resolution
        # img = nibabel.Nifti1Image(img.get_fdata(), img.affine, self._nifti_header)
        img.set_qform(img.affine, code='mni')

        if resample_to_mni152:
            img = resample_to_img(img, self._mni152_mri)

        if return_image:
            return img

        nibabel.save(img, nii_name)

    def save_stc(self, stc, stc_name):
        '''
        Save the source estimate.
        '''

        stc.save(stc_name)