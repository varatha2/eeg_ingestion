from __future__ import print_function

import numpy as np
import bisect
import re
from .process_mef import mef_to_mne
from labeling.sleep_staging import stage_yasa

import mne
from mne.time_frequency import psd_welch
from mne.preprocessing import (ICA, compute_proj_ecg, compute_proj_eog)
from mne_features.univariate import compute_samp_entropy as entropy

# FOOOF imports
from fooof import FOOOFGroup
from fooof.analysis import get_band_peak_fg

mne.set_log_level(verbose='ERROR')

_EPOCH_LEN = 10
_SAMP_FREQ = 256.0

db_channels_1 = ['fp1', 'f3', 'c3', 'p3', 'fp1', 'f7', 't7', 'p7',
                 'fp2', 'f4', 'c4', 'p4', 'fp2', 'f8', 't8', 'p8']

db_channels_2 = ['f3', 'c3', 'p3', 'o1', 'f7', 't7', 'p7', 'o1',
                 'f4', 'c4', 'p4', 'o2', 'f8', 't8', 'p8', 'o2']


def is_eeg_channel(ch):
    '''
    Determine if channel name is EEG channel.
    :param ch: string channel name
    :return: True if channel name is EEG channel
    '''

    # only exception is O2SAT because it can be confused with O2
    if 'o2' in ch.lower() and 'sat' in ch.lower():
        return False

    # use EEG channel names in the standard 10-20 montage
    std_montage = mne.channels.make_standard_montage('standard_1020')
    _all_eeg_channels = [_ch.lower() for _ch in std_montage.ch_names]

    for _ch in _all_eeg_channels:
        if _ch in ch.lower():
            return True

    return False


# def synchronize_epochs_and_events(epoch_start_samples, event_onsets, event_names):
#     '''
#
#     Args:
#         epoch_start_samples: start samples of epochs
#         event_onsets: onset samples of events
#         event_names: names of events
#
#     Returns:
#         event_list: list events in each epoch
#     '''
#
#     for i in range(len(epoch_start_samples)):
#         onset = event_onsets_samples[i]
#         ind = bisect.bisect_left(epochs.events[:, 0].tolist(), onset)
#         if ind:
#             if onset <= (epochs.events[ind - 1, 0] + epoch_len * raw.info['sfreq']):
#                 event_list[ind - 1].append(str(event_names[i]).strip().lower())

def remove_artifacts_SSP(epoch):
    '''
    Function to remove artifacts using the signal subspace projection method.
    Args:
        epoch: the epoch for artifact removal

    Returns: cleaned epoch

    '''

    ecg_channels = [ch for ch in epoch.ch_names if 'ecg' in ch.lower()]
    eye_channels = [ch for ch in epoch.ch_names if 'fpz' in ch.lower()]

    epoch_data = np.squeeze(epoch.get_data(), axis=0)
    epoch_raw = mne.io.RawArray(epoch_data, epoch.info)

    if ecg_channels:
        for ecg_channel in ecg_channels:
            ecg_projs, _ = compute_proj_ecg(epoch_raw, ch_name=ecg_channel, n_grad=0, n_mag=0, n_eeg=1,
                                            n_jobs=1, reject=None)

            if not ecg_projs is None:
                epoch_raw.add_proj(ecg_projs)

    if eye_channels:
        for eye_channel in eye_channels:
            eog_projs, _ = compute_proj_eog(epoch_raw, ch_name=eye_channel, n_grad=0, n_mag=0, n_eeg=1,
                                            n_jobs=1, reject=None)
            if not eog_projs is None:
                epoch_raw.add_proj(eog_projs)

    epoch_raw.apply_proj()

    return mne.EpochsArray(np.expand_dims(epoch_raw.get_data(), axis=0), epoch.info)


def remove_artifacts_ICA(epoch, ica_components=None, foof_freq_range=(1, 30), alpha_range=(8, 13),
                         muscle_freq_range=(15, 30), muscle_low_freq=20):
    '''
    Remove muscle and eye artifacts using specified EOG, EMG channels as reference.
    Args:
        epoch:
        ica_components:

    Returns:
        cleaned epoch
    '''

    reconst = epoch.copy()

    ica = ICA(n_components=ica_components, method='infomax', max_iter=1000)
    ica.fit(epoch)

    ## remove artifacts based on known channels
    emg_channels = [ch for ch in epoch.ch_names if 'emg' in ch.lower()]
    ecg_channels = [ch for ch in epoch.ch_names if 'ecg' in ch.lower()]
    eog_channels = [ch for ch in epoch.ch_names if 'fp' in ch.lower()]

    used_channels = eog_channels + ecg_channels

    if emg_channels:
        emg_entropy = entropy(np.squeeze(epoch.get_data(picks=emg_channels), axis=0))
        if not (emg_entropy < 0.2).any():
            used_channels += emg_channels
    # Now that we have our noise channels, we run the separate algorithm.
    bad_comps, scores = ica.find_bads_ref(epoch, ch_name=used_channels, method="separate", threshold=2.0)

    # print(bad_comps)

    ica_sources = ica.get_sources(epoch)

    psds, freqs = mne.time_frequency.psd_welch(ica_sources, fmin=1., fmax=30., picks='all')

    fg = FOOOFGroup(peak_width_limits=[1, 6], min_peak_height=0.15,
                    peak_threshold=2., max_n_peaks=6, verbose=False)

    # Fit the power spectrum model across all channels
    fg.fit(freqs, np.squeeze(psds), foof_freq_range)

    cfs = fg.get_params('peak_params', 'CF')
    exps = fg.get_params('aperiodic_params', 'exponent')

    alpha_pow = get_band_peak_fg(fg, alpha_range)[:, 1]
    muscle_pow = get_band_peak_fg(fg, muscle_freq_range)[:, 1]

    fooof_bads = np.where(np.array((exps < 0.5) & ((alpha_pow < 0.5) | np.isnan(alpha_pow)) & (muscle_pow > 0)))[0]
    muscle_bads = set(cfs[(cfs[:, 0] > muscle_low_freq), 1]) - set(np.where(alpha_pow > 0.5)[0])

    # ica.exclude = list(set(bad_comps + list(fooof_bads) + list(beta_peak_bads)))
    ica.exclude = list(set(bad_comps + list(fooof_bads) + list(muscle_bads)))
    ica.apply(reconst)

    return reconst


def create_double_banana_montage(epochs):
    '''
    Create a double banana montage from referential/recorded EEG.
    https://eegatlas-online.com/index.php/en/montages/bipolar/double-banana
    Args:
        epochs: MNE epochs to convert.

    Returns:
        epochs_db = double_banana epochs.

    '''

    check_1 = all(item in epochs.ch_names for item in db_channels_1)
    check_2 = all(item in epochs.ch_names for item in db_channels_2)
    # print(epochs.ch_names)
    # print(db_channels_1)
    # print(check_1)

    if not (check_1 and check_2):
        return None

    db_data = epochs.get_data(picks=db_channels_1) - epochs.get_data(picks=db_channels_2)
    db_ch_names = [ch1+'-'+ch2 for (ch1, ch2) in zip(db_channels_1, db_channels_2)]
    mne_info = mne.create_info(db_ch_names, ch_types=['eeg']*len(db_ch_names), sfreq=epochs.info['sfreq'])
    return mne.EpochsArray(db_data, mne_info)


def standardize_eeg(eeg_path, sample_rate=_SAMP_FREQ, apply_filter=True, bandpass = (0.5, 45.), avg_ref=False,
                    remove_artifacts=True, reject_bad=True, epoch_rule='fixed', epoch_len=_EPOCH_LEN,
                    score_sleep=False, common_ref=False, ref_ch=None, eye_channels=None, ecg_channels=None):

    '''
    Process EEG recordings and return MNE epochs object.
    :param eeg_path: path to EEG recording
    :param sample_rate: required sample rate (EEG will be resampled to this rate)
    :param apply_filter: perform filtering or not
    :param bandpass: frequencies for bandpass filtering
    :param remove_artifacts: perform artifact removal or not (ECG and Eye)
    :param avg_ref: perform average reference or not
    :param reject_bad: reject bad epochs or not
    :param epoch_rule: rule for selecting epochs
    :param epoch_len: length of epochs (in seconds)
    :param score_sleep: whether to score sleep (applies only to fixed length epochs)
    :param common_ref: whether to apply common reference
    :param ref_ch: channel to use for common reference
    :param eye_channels: eye channel to use for artifact removal
    :param ecg_channels: ecg channels to use for artifact removal
    :return: MNE epochs object processed with chosen preprocessing options, events (and sleep scores if score_sleep)

    '''

    # Convert EEG to MNE object for further processing
    if '.mefd' in eeg_path:
        raw = mef_to_mne(eeg_path, '', sample_rate)
    elif eeg_path.endswith('.edf'):
        raw = mne.io.read_raw_edf(eeg_path, preload=True)
    elif eeg_path.endswith('.lay'):
        raw = mne.io.read_raw_persyst(eeg_path, preload=True)
    elif eeg_path.endswith('.set'):
        raw = mne.io.read_raw_eeglab(eeg_path, preload=True)
    else:
        raw = None

    if not raw:
        return None, None

    # String channel names of extensions such as -ref and -org, and replace old channel names with new ones
    new_names = dict((ch_name, re.sub(r"-[a-zA-Z]+", "", ch_name).lower().replace('t3', 't7').replace('t4', 't8')
                      .replace('t5', 'p7').replace('t6', 'p8').replace('eeg ', ''))
        for ch_name in raw.ch_names)
    raw.rename_channels(new_names)

    # setting channel types
    channel_names = np.array(raw.ch_names)
    eeg_ids = [is_eeg_channel(ch) for ch in channel_names]
    mapping = {i: 'eeg' for i in channel_names[np.array(eeg_ids)]}
    raw.set_channel_types(mapping=mapping)
    mapping = {i: 'misc' for i in channel_names[~np.array(eeg_ids)]}
    raw.set_channel_types(mapping=mapping)


    # standardize annotations
    # event_names = [str(event).strip().lower().replace('(created by persyst)', '')
    #                    .replace('correct', 'eyes closed').replace('wrong', 'eyes closed')
    #                    .replace('100-7', 'eyes closed').replace('93-7', 'eyes closed')
    #                    .replace('president?', 'eyes closed').replace('president', 'eyes closed')
    #                    .replace('13 x 5', 'eyes closed') for event in raw.annotations.description]
    event_names = [str(event).strip().lower().replace('(created by persyst)', '')
                   for event in raw.annotations.description]
    onsets = raw.annotations.onset
    durations = raw.annotations.duration
    annotations = mne.Annotations(onsets, durations, event_names)
    raw.set_annotations(annotations)

    # resample to sample_rate
    if (raw.info['sfreq'] != sample_rate):
        raw = raw.resample(sfreq=sample_rate)

    if apply_filter:
        # low pass and notch filters
        raw = raw.filter(l_freq=bandpass[0], h_freq=bandpass[1], picks=['eeg', 'misc'])
        line_freqs = range(60, int(raw.info['sfreq']//2), 60)
        raw = raw.notch_filter(freqs=line_freqs, picks=['eeg', 'misc'])

    if avg_ref:
        mne.set_eeg_reference(raw, ref_channels='average', copy=False, projection=False, ch_type='eeg')

    if common_ref:
        mne.set_eeg_reference(raw, ref_channels=ref_ch, copy=False, projection=False, ch_type='eeg')

    sleep_stages = []
    sleep_stage_onsets = []
    if score_sleep:
        try:
            sleep_stages = stage_yasa(eeg_data=raw, epochs=False)
            sleep_stage_markers = mne.make_fixed_length_events(raw, duration=30.0)  # sleep stages are scored every 30s
            sleep_stage_onsets = sleep_stage_markers[:, 0]
        except:
            print('Error scoring sleep %s' % eeg_path)

    if remove_artifacts:
        if ecg_channels:
            val_ecg_channels = []
            for ecg_channel in ecg_channels:
                if ecg_channel in raw.ch_names:
                    val_ecg_channels.append(ecg_channel)
        else:
            val_ecg_channels = [ch for ch in raw.ch_names if 'ecg' in ch.lower()]
        if eye_channels:
            val_eye_channels = []
            for eye_channel in eye_channels:
                if eye_channel in raw.ch_names:
                    val_eye_channels.append(eye_channel)
        else:
            val_eye_channels = [ch for ch in raw.ch_names if 'fpz' in ch.lower()]
        # eye_channels = eye_channels + [ch for ch in raw.ch_names if 'eye' in ch.lower()]
        # eye_channels = [ch for ch in raw.ch_names if 'eye' in ch.lower()]

        # combined_data = mne.concatenate_epochs([epochs[i] for i in range(len(epochs))], add_offset=False)
        # split_data = np.vsplit(combined_data.get_data(), len(combined_data))
        # full_data = np.squeeze(np.dstack(split_data), axis=0)
        # good_raw = mne.io.RawArray(full_data, epochs.info)

        raw.del_proj()

        if val_ecg_channels:
            for ecg_channel in val_ecg_channels:
                ecg_projs, _ = compute_proj_ecg(raw, ch_name=ecg_channel, n_grad=0, n_mag=0, n_eeg=1,
                                                n_jobs=1, reject=None)

                if not ecg_projs is None:
                    raw.add_proj(ecg_projs)

        if val_eye_channels:
            for eye_channel in val_eye_channels:
                eog_projs, _ = compute_proj_eog(raw, ch_name=eye_channel, n_grad=0, n_mag=0, n_eeg=1,
                                                n_jobs=1, reject=None)
                if not eog_projs is None:
                    raw.add_proj(eog_projs)

        raw.apply_proj()

    # epoch creation
    if epoch_rule == 'fixed':
        epoch_markers = mne.make_fixed_length_events(raw, duration=epoch_len)
        epochs = mne.Epochs(raw, events=epoch_markers, tmin=0, tmax=epoch_len, baseline=(None, None), preload=True)
    elif epoch_rule == 'eyes closed':
        events, event_ids = mne.events_from_annotations(raw, event_id={'eyes closed': 1})
        epochs = mne.Epochs(raw, events, event_ids, tmin=0, tmax=epoch_len, baseline=(None, None)).drop_bad()
        epochs = epochs.load_data()
    else:
        epoch_markers = mne.make_fixed_length_events(raw, duration=epoch_len)
        epochs = mne.Epochs(raw, events=epoch_markers, tmin=0, tmax=epoch_len, baseline=(None, None), preload=True)

    # Reject bad epochs
    # Rule: Total signal power in Cz (22) is outside of 2 STDs
    if reject_bad:
        psds, freqs = psd_welch(epochs, fmin=bandpass[0], fmax=bandpass[1])
        tot_power = np.log(np.sum(psds[:, [ch.lower() for ch in epochs.ch_names].index('c3'), :], axis=-1))
        epochs = epochs[(tot_power > (np.mean(tot_power) - 2 * np.std(tot_power))) &
                        (tot_power < (np.mean(tot_power) + 2 * np.std(tot_power)))]
        # if score_sleep:
        #     sleep_stages = sleep_stages[(tot_power > (np.mean(tot_power) - 2 * np.std(tot_power))) &
        #                 (tot_power < (np.mean(tot_power) + 2 * np.std(tot_power)))]

    if epoch_rule == 'eyes closed':
        event_list = ['eyes closed' for _ in range(len(epochs))]
        return epochs, event_list

    # Find events corresponding to each epoch in the good data
    event_list = [[] for _ in range(len(epochs))]

    event_onsets_samples = (np.array(raw.annotations.onset) * raw.info['sfreq']).astype(int)
    event_names = raw.annotations.description

    for i in range(len(event_onsets_samples)):
        onset = event_onsets_samples[i]
        ind = bisect.bisect_left(epochs.events[:, 0].tolist(), onset)
        if ind:
            if onset <= (epochs.events[ind - 1, 0] + epoch_len * raw.info['sfreq']):
                event_list[ind - 1].append(str(event_names[i]).strip().lower())

    if score_sleep:
        sleep_stage_list = ['NA' for _ in range(len(epochs))]
        event_onsets_samples = epochs.events[:, 0].tolist()

        for i in range(len(event_onsets_samples)):
            onset = event_onsets_samples[i]
            ind = bisect.bisect_left(sleep_stage_onsets, onset)
            if ind:
                sleep_stage_list[i] = sleep_stages[ind - 1]

        return epochs, event_list, sleep_stage_list

    return epochs, event_list
