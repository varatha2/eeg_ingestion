'''Simple approach for sleep staging routine EEGs using the YASA approach.
https://github.com/raphaelvallat/yasa
'''

import numpy as np
import mne
import yasa
from feature_extraction.eeg_stats import count_eye_blinks
from mne.time_frequency import psd_welch
from mne_features.univariate import compute_samp_entropy as entropy

# FOOOF imports
from fooof import FOOOFGroup
from fooof.bands import Bands
from fooof.analysis import get_band_peak_fg

import tensortools as tt

yasa_behavioral_state_mapping = {'W': 0, 'N1': 1, 'N2': 2, 'N3': 3, 'R': 4}
valid_sleep_stages = ['W', 'N1', 'N2', 'N3', 'R']

def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)


def stage_yasa(eeg_data, epochs=True):
    """
    Stage sleep using YASA algorithm.
    Args:
        epochs: whether eeg_data is MNE Epochs object
        eeg_data: MNE Epochs or Raw object

    Returns:

    """

    if epochs:
        combined_data = mne.concatenate_epochs([eeg_data[i] for i in range(len(eeg_data))], add_offset=False)
        split_data = np.vsplit(combined_data.get_data(), len(combined_data))
        full_data = np.squeeze(np.dstack(split_data), axis=0)
        full_raw = mne.io.RawArray(full_data, eeg_data.info)
    else:
        full_raw = eeg_data

    eye_channels = [ch for ch in eeg_data.ch_names if 'eye' in ch.lower()] + \
                   [ch for ch in eeg_data.ch_names if 'fp' in ch.lower()]

    emg_name = None
    emg_channels = [ch for ch in eeg_data.ch_names if 'emg' in ch.lower()] + \
                   [ch for ch in eeg_data.ch_names if 'chin' in ch.lower()]
    if emg_channels:
        emg_name = emg_channels[0]

    # try:
    sls = yasa.SleepStaging(full_raw, eeg_name="c4",
                            eog_name=eye_channels[0],
                            emg_name=emg_name)  # hard coding c4 coz it is one of the 19 channels.
    y_pred = sls.predict()
    return y_pred
    # return np.vectorize(yasa_behavioral_state_mapping.get)(y_pred)
    # except Exception as e:
    #     print(e)
    #     print('Error scoring sleep: eye-' + str(eye_channels[0]) + ', emg-' + str(emg_name))
    #     return [-1]*len(epochs)

def calc_fragmentation_metrics(sleep_stage_list):
    '''
    Calculates sleep fragmentation index and state transition density.
    Args:
        sleep_stage_list:

    Returns:
        sfi: sleep fragmentation index
        std: state transition density
    '''

    if 'NA' in sleep_stage_list:
        sleep_stage_list = [st for st in sleep_stage_list if st in valid_sleep_stages]

    if not sleep_stage_list:
        return -1, -1

    sleep_stages_numeric = np.vectorize(yasa_behavioral_state_mapping.get)(sleep_stage_list)
    _, probs = yasa.transition_matrix(sleep_stages_numeric)

    sfi = -1
    std = -1
    if len(probs) > 1:
        sfi = np.diag(probs.loc[1:, 1:]).mean()
        std = np.nonzero(np.diff(sleep_stages_numeric))[0].size / sleep_stages_numeric.size

    return sfi, std


def find_eyes_closed_epochs_alpha_peak(epochs, eeg_channel_names, sleep_stages, event_list, max_epochs=6, min_epochs=3):
    '''
    Identify eyes closed epochs in a series of epochs.
    Args:
        epochs:
        eeg_channel_names:
        sleep_stages:
        event_list:
        max_epochs: max requested number of epochs
        min_epochs: min requested number of epochs

    Returns:
        ec_epochs
    '''

    eoc_epoch_ids = [list_contains_substr(curr_list, 'eyes') for curr_list in event_list]
    true_idx = np.where(eoc_epoch_ids)[0]
    if len(true_idx) > 0:
        start_idx = true_idx[0]
    else:
        start_idx = min(30, len(epochs) - 1)

    epochs = epochs[start_idx:]
    sleep_stages = sleep_stages[start_idx:]
    sleep_stages_numeric = np.vectorize(yasa_behavioral_state_mapping.get)(sleep_stages)
    all_wake_epochs = epochs[sleep_stages_numeric == 0]

    all_wake_epochs = all_wake_epochs[:300] # use first 100 wake epochs

    if (len(all_wake_epochs) > max_epochs):
        blink_counts, thresh = count_eye_blinks(all_wake_epochs)
        ec_epochs = all_wake_epochs[blink_counts == 0]

        if (len(ec_epochs) > 0):
            ## remove epochs that have low entropy channels
            val_ec = []
            for i in range(len(ec_epochs)):
                epoch_data = np.squeeze(ec_epochs[i].get_data(picks=eeg_channel_names), axis=0)
                ent_vals = entropy(epoch_data)
                val_ec.append(1 if np.sum(ent_vals < 0.4) == 0 else 0) # 0.4 is empirically chosen threshold

            ec_epochs = ec_epochs[np.array(val_ec) == 1]

            # psds, freqs = psd_welch(ec_epochs, fmin=8., fmax=13.)
            # tot_alpha_power = np.sum(psds, axis=-1, keepdims=False)
            # alpha_occ = np.reshape(tot_alpha_power[:, ec_epochs.info['ch_names'].index('o1')] \
            #                        + tot_alpha_power[:, ec_epochs.info['ch_names'].index('o2')], [-1])
            # alpha_fron = np.reshape(tot_alpha_power[:, ec_epochs.info['ch_names'].index('f3')] \
            #                         + tot_alpha_power[:, ec_epochs.info['ch_names'].index('f4')], [-1])

            if len(ec_epochs) == 0:
                print('No EC epochs found..')
                return []

            psds, freqs = psd_welch(ec_epochs, fmin=1., fmax=30.)
            occ_psds = (psds[:, ec_epochs.info['ch_names'].index('o1'), :] +
                       psds[:, ec_epochs.info['ch_names'].index('o2'), :]) / 2.0

            if not occ_psds.ndim == 2:
                occ_psds = np.squeeze(occ_psds, axis=1)

            fg = FOOOFGroup(peak_width_limits=[1, 3], min_peak_height=0.5,
                            peak_threshold=3., max_n_peaks=6, verbose=False)

            # Define the frequency range to fit
            freq_range = [1, 30]

            # Fit the power spectrum model across all channels
            fg.fit(freqs, occ_psds, freq_range)

            exps = fg.get_params('aperiodic_params', 'exponent')

            alpha_pow = get_band_peak_fg(fg, [8, 13])[:, 1]  ## (each row is [CF, PW, BW])

            # print(alpha_pow)

            alpha_exists = alpha_pow[(alpha_pow > 0.5) & (exps >= 0.5)]
            epochs_with_alpha = ec_epochs[(alpha_pow > 0.5) & (exps >= 0.5)]

            index_array = np.argsort(alpha_exists)
            val_epochs = epochs_with_alpha[index_array]

            if len(val_epochs) < min_epochs:
                print('Not enough EC epochs found..')
                return []

            return val_epochs[-max_epochs:]

            # index_array = np.argsort(alpha_occ)
            # ec_epochs = ec_epochs[index_array]

            # alpha_ec_zs = np.divide(alpha_occ[index_array], alpha_fron[index_array])
            #
            # if (np.sum(alpha_ec_zs > 2.5) == 0):
            #     print('Valid EC epochs not available')
            #     return []
            #
            # ec_epochs = ec_epochs[alpha_ec_zs > 2.5]
            # n_epochs = min(n_epochs, len(ec_epochs))
            # print(index_array)
            # return ec_epochs[-n_epochs:]
        else:
            print('Not enough EC epochs found..')
            return []
    else:
        print('Not enough wake epochs found..')
        return []


def find_eyes_closed_epochs_alpha_pow(epochs, eeg_channel_names, sleep_stages, event_list,
                                      remove_on_entropy=True, max_epochs=6, min_epochs=3,
                                      full_record=True, use_first_hour=False):
        '''
        Identify eyes closed epochs in a series of epochs.
        Args:
            epochs:
            eeg_channel_names:
            sleep_stages:
            event_list:
            max_epochs: max requested number of epochs
            min_epochs: min requested number of epochs
            remove_on_entropy: whether to remove epochs that have low entropy
            full_record: whether the EEG record is a full record or a clipped record
            use_first_hour: whether to use only first hour data (to speed up computation)

        Returns:
            ec_epochs
        '''

        epoch_len = int(epochs[0].get_data().shape[2] // epochs.info['sfreq'])

        if full_record:
            eoc_epoch_ids = [list_contains_substr(curr_list, 'eyes') for curr_list in event_list]
            true_idx = np.where(eoc_epoch_ids)[0]
            if len(true_idx) > 0:
                start_idx = true_idx[0]
            else:
                start_idx = min(300 // epoch_len, len(epochs) - 1)
        else:
            start_idx = min(30 // epoch_len, len(epochs) - 1)

        epochs = epochs[start_idx:]
        sleep_stages = sleep_stages[start_idx:]
        sleep_stages_numeric = np.vectorize(yasa_behavioral_state_mapping.get)(sleep_stages)
        all_wake_epochs = epochs[sleep_stages_numeric == 0]

        if use_first_hour:
            all_wake_epochs = all_wake_epochs[:(3600 // epoch_len)]

        if (len(all_wake_epochs) > max_epochs):
            blink_counts, thresh = count_eye_blinks(all_wake_epochs)
            ec_epochs = all_wake_epochs[blink_counts == np.min(blink_counts)]

            if (len(ec_epochs) > 0):

                if remove_on_entropy:
                    ## remove epochs that have low entropy channels
                    val_ec = []
                    for i in range(len(ec_epochs)):
                        epoch_data = np.squeeze(ec_epochs[i].get_data(picks=eeg_channel_names), axis=0)
                        ent_vals = entropy(epoch_data)
                        val_ec.append(1 if np.sum(ent_vals < 0.4) == 0 else 0)  # 0.4 is empirically chosen threshold

                    ec_epochs = ec_epochs[np.array(val_ec) == 1]

                if len(ec_epochs) < min_epochs:
                    print('Not enough EC epochs found..')
                    return []

                psds, freqs = psd_welch(ec_epochs, fmin=8., fmax=13.)
                tot_alpha_power = np.sum(psds, axis=-1, keepdims=False)
                alpha_occ = np.reshape(tot_alpha_power[:, ec_epochs.info['ch_names'].index('o1')] \
                                       + tot_alpha_power[:, ec_epochs.info['ch_names'].index('o2')], [-1])

                index_array = np.argsort(alpha_occ)
                ec_epochs = ec_epochs[index_array]

                return ec_epochs[-max_epochs:]

                # alpha_ec_zs = np.divide(alpha_occ[index_array], alpha_fron[index_array])
                #
                # if (np.sum(alpha_ec_zs > 2.5) == 0):
                #     print('Valid EC epochs not available')
                #     return []
                #
                # ec_epochs = ec_epochs[alpha_ec_zs > 2.5]
                # n_epochs = min(n_epochs, len(ec_epochs))
                # print(index_array)
                # return ec_epochs[-n_epochs:]
            else:
                print('Not enough EC epochs found..')
                return []
        else:
            print('Not enough wake epochs found..')
            return []


def find_wake_epochs_alpha_pow(epochs, eeg_channel_names, sleep_stages, event_list, max_epochs=6, min_epochs=3,
                               remove_on_entropy=True, full_record=True):
    '''
    Identify eyes closed epochs in a series of epochs.
    Args:
        epochs:
        eeg_channel_names:
        sleep_stages:
        event_list:
        max_epochs: max requested number of epochs
        min_epochs: min requested number of epochs
        remove_on_entropy: whether to remove epochs that have low entropy
        full_record: whether the EEG record is a full record or a clipped record

    Returns:
        ec_epochs
    '''

    epoch_len = int(epochs[0].get_data().shape[2]//epochs.info['sfreq'])

    if full_record:
        eoc_epoch_ids = [list_contains_substr(curr_list, 'eyes') for curr_list in event_list]
        true_idx = np.where(eoc_epoch_ids)[0]
        if len(true_idx) > 0:
            start_idx = true_idx[0]
        else:
            start_idx = min(300//epoch_len, len(epochs) - 1)
    else:
        start_idx = min(30//epoch_len, len(epochs) - 1)

    epochs = epochs[start_idx:]
    sleep_stages = sleep_stages[start_idx:]
    sleep_stages_numeric = np.vectorize(yasa_behavioral_state_mapping.get)(sleep_stages)
    all_wake_epochs = epochs[sleep_stages_numeric == 0]

    all_wake_epochs = all_wake_epochs  # use first 100 wake epochs

    if (len(all_wake_epochs) > max_epochs):

        if remove_on_entropy:
            ## remove epochs that have low entropy channels
            val_wake = []
            for i in range(len(all_wake_epochs)):
                epoch_data = np.squeeze(all_wake_epochs[i].get_data(picks=eeg_channel_names), axis=0)
                ent_vals = entropy(epoch_data)
                val_wake.append(1 if np.sum(ent_vals < 0.4) == 0 else 0)  # 0.4 is empirically chosen threshold

            val_wake_epochs = all_wake_epochs[np.array(val_wake) == 1]
        else:
            val_wake_epochs = all_wake_epochs

        if len(val_wake_epochs) < min_epochs:
            print('Not enough EC epochs found..')
            return []

        psds, freqs = psd_welch(val_wake_epochs, fmin=1., fmax=30.)
        tot_alpha_power = np.sum(psds[:,:, 7:13], axis=-1, keepdims=False)
        alpha_occ = np.reshape(tot_alpha_power[:, val_wake_epochs.info['ch_names'].index('o1')] \
                               + tot_alpha_power[:, val_wake_epochs.info['ch_names'].index('o2')], [-1])

        index_array = np.argsort(alpha_occ)
        val_wake_epochs = val_wake_epochs[index_array]

        return val_wake_epochs[-max_epochs:]

        # psds_min = np.min(np.reshape(psds, [-1]))
        # psds_max = np.max(np.reshape(psds, [-1]))
        # trans_psds = (psds - psds_min) / (psds_max - psds_min)
        #
        # U = tt.ncp_hals(trans_psds, rank=6, verbose=False, random_state=0)
        # factors_tt = U.factors.factors
        #
        # muscle_factor = np.argmax(np.sum(factors_tt[2][12:25, :], axis=0)/np.sum(factors_tt[2][7:12, :], axis=0))
        # muscle_loadings = factors_tt[0][:, muscle_factor]
        #
        # index_array = np.argsort(muscle_loadings/alpha_occ)
        # val_wake_epochs = val_wake_epochs[index_array]
        #
        # return val_wake_epochs[:max_epochs]

        # alpha_ec_zs = np.divide(alpha_occ[index_array], alpha_fron[index_array])
        #
        # if (np.sum(alpha_ec_zs > 2.5) == 0):
        #     print('Valid EC epochs not available')
        #     return []
        #
        # ec_epochs = ec_epochs[alpha_ec_zs > 2.5]
        # n_epochs = min(n_epochs, len(ec_epochs))
        # print(index_array)
        # return ec_epochs[-n_epochs:]
    else:
        print('Not enough wake epochs found..')
        return []