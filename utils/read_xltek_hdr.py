from binascii import hexlify
from datetime import datetime
from pathlib import Path

from struct import unpack


def _make_str(t_in):
    t_out = []
    for t in t_in:
        if t == b'\x00':
            break
        t_out.append(t.decode('utf-8'))
    return ''.join(t_out)

def read_hdr_file(ktlx_file):
    """Reads header of one KTLX file.
    Parameters
    ----------
    ktlx_file : Path
        name of one of the ktlx files inside the directory (absolute path)
    Returns
    -------
    dict
        dict with information about the file
    Notes
    -----
    p.3: says long, but python-long requires 8 bytes, so we use f.read(4)
    GUID is correct, BUT little/big endian problems somewhere
    """
    with ktlx_file.open('rb') as f:

        hdr = {}
        assert f.tell() == 0

        hdr['file_guid'] = hexlify(f.read(16))
        hdr['file_schema'], = unpack('<H', f.read(2))
        if not hdr['file_schema'] in (1, 3, 7, 8, 9):
            raise NotImplementedError('Reading header not implemented for ' +
                                      'file_schema ' + str(hdr['file_schema']))

        hdr['base_schema'], = unpack('<H', f.read(2))
        if not hdr['base_schema'] == 1:  # p.3: base_schema 0 is rare, I think
            raise NotImplementedError('Reading header not implemented for ' +
                                      'base_schema ' + str(hdr['base_schema']))

        hdr['creation_time'] = datetime.fromtimestamp(unpack('<i',
                                                             f.read(4))[0])
        hdr['patient_id'], = unpack('<i', f.read(4))
        hdr['study_id'], = unpack('<i', f.read(4))
        hdr['pat_last_name'] = _make_str(unpack('c' * 80, f.read(80)))
        hdr['pat_first_name'] = _make_str(unpack('c' * 80, f.read(80)))
        hdr['pat_middle_name'] = _make_str(unpack('c' * 80, f.read(80)))
        hdr['patient_id'] = _make_str(unpack('c' * 80, f.read(80)))
        assert f.tell() == 352

        if hdr['file_schema'] >= 7:
            hdr['sample_freq'], = unpack('<d', f.read(8))
            n_chan, = unpack('<i', f.read(4))
            hdr['num_channels'] = n_chan
            # hdr['deltabits'], = unpack('<i', f.read(4))
            # hdr['phys_chan'] = unpack('<' + 'i' * hdr['num_channels'],
            #                           f.read(hdr['num_channels'] * 4))

            f.seek(4464)
            hdr['headbox_type'] = unpack('<' + 'i' * 4, f.read(16))
            hdr['headbox_sn'] = unpack('<' + 'i' * 4, f.read(16))
            hdr['headbox_sw_version'] = _make_str(unpack('c' * 40, f.read(40)))
            hdr['dsp_hw_version'] = _make_str(unpack('c' * 10, f.read(10)))
            hdr['dsp_sw_version'] = _make_str(unpack('c' * 10, f.read(10)))
            hdr['discardbits'], = unpack('<i', f.read(4))

        # if hdr['file_schema'] >= 8:
        #     hdr['shorted'] = unpack('<' + 'h' * 1024, f.read(2048))[:n_chan]
        #     hdr['frequency_factor'] = unpack('<' + 'h' * 1024,
        #                                      f.read(2048))[:n_chan]
    return hdr


def return_header(xltek_name):

    foldername = Path(xltek_name)

    hdr = []
    for erd_file in foldername.glob('*.erd'):
        try:
            hdr.append(read_hdr_file(erd_file))
            # hdr['erd'] = read_hdr_file(erd_file)
            # we need this to look up stc
            # hdr['erd'].update({'filename': erd_file.stem})
            # break

        except (FileNotFoundError, PermissionError):
            pass

    return sorted(hdr, key=lambda k: k['creation_time'])
