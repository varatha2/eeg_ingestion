'''Util for creating and reading TF Examples based on scalp EEG.'''

import tensorflow as tf

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _floatlist_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def create_eeg_example(subject_id, age, eeg_guid, nsamples, nchannels, chan_names, chan_types, vectorized_data,
                       psd_features, topomap_data, behavioral_state, behavioral_state_yasa, pathological):
    ''' Creates a TF Example using eeg and additional features.'''

    feature = {
        'Subject/age/value': _int64_feature(int(age)),
        'Subject/ID': _bytes_feature(str(subject_id).encode()),
        'EEG/ID': _bytes_feature(str(eeg_guid).encode()),
        'EEG/nsamples': _int64_feature(nsamples),
        'EEG/nchannels': _int64_feature(nchannels),
        'EEG/chan_names': _bytes_feature(str(chan_names).encode()),
        'EEG/chan_types': _bytes_feature(str(chan_types).encode()),
        #     'EEG/chan_names': _bytes_feature([str.encode(chan_name) for chan_name in chan_names]),
        #     'EEG/chan_names': _bytes_feature([str.encode(chan_type) for chan_type in chan_types]),
        'EEG/data': _bytes_feature(tf.compat.as_bytes(vectorized_data.tostring())),
        'EEG/features/PSD': _bytes_feature(tf.compat.as_bytes(psd_features.tostring())),
        'EEG/features/topomap': _bytes_feature(tf.compat.as_bytes(topomap_data.tostring())),
        'EEG/labels/behavioral': _int64_feature(behavioral_state),
        'EEG/labels/behavioral/yasa': _int64_feature(behavioral_state_yasa),
        'EEG/labels/pathlogical': _int64_feature(pathological)
    }

    # Return an example protocol buffer.
    return tf.train.Example(features=tf.train.Features(feature=feature))


def parse_eeg_example(serialized_eeg_example):
    ''' Parses an eeg TF Example and returns data and additional features.
    (Should be run within a TF session.)
    '''

    feature = {
        'Subject/age/value': tf.io.FixedLenFeature([], tf.int64),
        'Subject/ID': tf.io.FixedLenFeature([], tf.string),
        'EEG/ID': tf.io.FixedLenFeature([], tf.string),
        'EEG/nsamples': tf.io.FixedLenFeature([], tf.int64),
        'EEG/nchannels': tf.io.FixedLenFeature([], tf.int64),
        'EEG/chan_names': tf.io.FixedLenFeature([], tf.string),
        'EEG/chan_types': tf.io.FixedLenFeature([], tf.string),
        'EEG/data': tf.io.FixedLenFeature([], tf.string),
        'EEG/features/PSD': tf.io.FixedLenFeature([], tf.string),
        'EEG/features/topomap': tf.io.FixedLenFeature([], tf.string),
        'EEG/labels/behavioral': tf.io.FixedLenFeature([], tf.int64),
        'EEG/labels/behavioral/yasa': tf.io.FixedLenFeature([], tf.int64),
        'EEG/labels/pathlogical': tf.io.FixedLenFeature([], tf.int64)
    }

    # Decode the record read by the reader
    features = tf.io.parse_single_example(serialized_eeg_example, features=feature)

    # Retrieve individual attributes
    age = tf.cast(features['Subject/age/value'], tf.int32)
    subject_id = tf.cast(features['Subject/ID'], tf.string)
    eeg_id = tf.cast(features['EEG/ID'], tf.string)
    nsamples = tf.cast(features['EEG/nsamples'], tf.int32)
    nchannels = tf.cast(features['EEG/nchannels'], tf.int32)
    chan_names = tf.cast(features['EEG/chan_names'], tf.string)
    chan_types = tf.cast(features['EEG/chan_types'], tf.string)
    vectorized_data = tf.io.decode_raw(features['EEG/data'], tf.float64)
    psd_features = tf.io.decode_raw(features['EEG/features/PSD'], tf.float64)
    topomap_data = tf.io.decode_raw(features['EEG/features/topomap'], tf.float64)
    behavioral_state = tf.cast(features['EEG/labels/behavioral'], tf.int32)
    behavioral_state_yasa = tf.cast(features['EEG/labels/behavioral/yasa'], tf.int32)
    pathological = tf.cast(features['EEG/labels/pathlogical'], tf.int32)

    return age, subject_id, eeg_id, nsamples, nchannels, chan_names, chan_types, \
           vectorized_data, psd_features, topomap_data, behavioral_state, behavioral_state_yasa, pathological
