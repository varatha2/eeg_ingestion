'''Utility to read tfrecord files and generate data for training and evaluation.'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf
from . import eeg_tfexample


def pad_fn(age, subject_id, eeg_id, nsamples, nchannels, chan_names, chan_types,
           vectorized_data, psd_features, behavioral_state, pathological, pad_channels=19):
    '''
    A tfdata map function that pads EEG data to have the same number of channels (default=19).
    Args:
        age:
        subject_id:
        eeg_id:
        nsamples:
        nchannels:
        chan_names:
        chan_types:
        vectorized_data:
        psd_features:
        behavioral_state:
        pathological:
        pad_channels:

    Returns:

    '''

    res_data = tf.reshape(tf.cast(vectorized_data, tf.float32), [nchannels, nsamples])
    res_psd = tf.reshape(tf.cast(psd_features, tf.float32), [nchannels, -1])

    pad_size = pad_channels - nchannels
    paddings = [[0, pad_size], [0, 0]]
    vectorized_data_new = tf.reshape(tf.pad(res_data, paddings, "CONSTANT"), [-1])

    psd_features_new = tf.reshape(tf.pad(res_psd, paddings, "CONSTANT"), [-1])

    nchannels_new = tf.cast(pad_channels, tf.int32)

    return age, subject_id, eeg_id, nsamples, nchannels_new, chan_names, chan_types, \
           vectorized_data_new, psd_features_new, behavioral_state, pathological


def order_fn(age, subject_id, eeg_id, nsamples, nchannels, chan_names, chan_types,
               vectorized_data, psd_features, behavioral_state, pathological):
    '''
    A tfdata map function that pads orders the data and features based on a fixed order of channels.
    Args:
        age:
        subject_id:
        eeg_id:
        nsamples:
        nchannels:
        chan_names:
        chan_types:
        vectorized_data:
        psd_features:
        behavioral_state:
        pathological:

    Returns:

    '''
    chs = ['fp1', 'f3', 'f7', 't7', 'p3', 'p7', 'o1', 'fp2', 'f4', 'f8', 't8',
           'p4', 'p8', 'o2', 'fpz', 'fz', 'cz', 'pz']
    chname_tensor = tf.strings.split(chan_names, sep=', ', result_type='RaggedTensor')

    gather_locs = []
    for ch in chs:
        curr_ind = tf.strings.regex_full_match(chname_tensor, ".*'{}'*$".format(ch))
        gather_locs.append(tf.where(curr_ind))

    data_res = tf.reshape(tf.cast(vectorized_data, tf.float32), [nchannels, nsamples])
    ord_data = tf.squeeze(tf.gather_nd(data_res, gather_locs), axis=1)
    psd_res = tf.reshape(tf.cast(psd_features, tf.float32), [nchannels, -1])
    ord_psd = tf.squeeze(tf.gather_nd(psd_res, gather_locs), axis=1)

    return age, subject_id, eeg_id, nsamples, tf.constant(len(chs)), tf.convert_to_tensor(chs), \
           ord_data, ord_psd, behavioral_state, pathological


def normal_filter_fn(age, subject_id, eeg_id, nsamples, nchannels, chan_names, chan_types,
                     vectorized_data, psd_features, behavioral_state, pathological,
                     allowed_labels=tf.constant([0, 10])):
    '''
    A tfdata filter function that filters tfexamples with pathological state 0 or 10.
    Args:
        age:
        subject_id:
        eeg_id:
        nsamples:
        nchannels:
        chan_names:
        chan_types:
        vectorized_data:
        psd_features:
        behavioral_state:
        pathological:
        allowed_labels:

    Returns:

    '''

    isallowed = tf.equal(allowed_labels, tf.cast(pathological, tf.int32))
    reduced = tf.reduce_sum(tf.cast(isallowed, tf.float32))
    return tf.greater(reduced, tf.constant(0.))


def beh_filter_fn(age, subject_id, eeg_id, nsamples, nchannels, chan_names, chan_types,
                  vectorized_data, psd_features, behavioral_state, pathological, discard=tf.constant([-1.])):
    '''
    A tfdata filter function that filters tfexamples with behavioral state other than -1.
    Args:
        age:
        subject_id:
        eeg_id:
        nsamples:
        nchannels:
        chan_names:
        chan_types:
        vectorized_data:
        psd_features:
        behavioral_state:
        pathological:
        discard:

    Returns:

    '''

    isallowed = tf.not_equal(discard, tf.cast(behavioral_state, tf.float32))
    reduced = tf.reduce_sum(tf.cast(isallowed, tf.float32))
    return tf.greater(reduced, tf.constant(0.))


def create_dataset(tfrecord_path, batch_size, num_examples=None, train=True,
                   pad_channels=None, normal_filter=False, bh_annotation_filter=True, order_data=False):
    '''
    Creates a tfrecord dataset batch input for model training.
    Args:
        tfrecord_path:
        batch_size:
        num_examples:
        train:
        pad_channels:
        normal_filter:
        bh_annotation_filter:
        order_data:

    Returns:

    '''

    if not isinstance(tfrecord_path, list):
        tfrecord_path = [tfrecord_path]

    # Gather all tf records
    data_files = []
    for path in tfrecord_path:
        data_files += tf.io.gfile.glob('%s-*' % path)

    # This works with arrays as well
    dataset = tf.data.TFRecordDataset(data_files)

    if train:
        # This dataset will go on forever
        dataset = dataset.repeat()

        # Set the number of datapoints you want to load and shuffle
        dataset = dataset.shuffle(num_examples)

    # Maps the parser on every filepath in the array. You can set the number of parallel loaders here
    dataset = dataset.map(eeg_tfexample.parse_eeg_example, num_parallel_calls=tf.data.experimental.AUTOTUNE)

    if pad_channels and isinstance(pad_channels, int):
        dataset = dataset.map(lambda x: pad_fn(x, pad_channels=pad_channels))

    if bh_annotation_filter:
        # filter examples that have labeled behavioral state
        dataset = dataset.filter(beh_filter_fn)

    if normal_filter:
        # filter examples that are not pathological
        dataset = dataset.filter(normal_filter_fn)

    if order_data:
        dataset = dataset.map(order_fn, num_parallel_calls=tf.data.experimental.AUTOTUNE)

    # Set the batchsize
    dataset = dataset.batch(batch_size)

    # prefetch
    dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

    # Create an iterator
    iterator = iter(dataset)

    # Create your tf representation of the iterator
    age, subject_id, eeg_id, nsamples, nchannels, chan_names, chan_types, \
    vectorized_data, psd_features, behavioral_state, pathological = iterator.get_next()

    batch_input_dict = {
        'age': tf.reshape(age, [batch_size, 1]),
        'subject_id': tf.reshape(subject_id, [batch_size, 1]),
        'eeg_id': tf.reshape(eeg_id, [batch_size, 1]),
        'nsamples': tf.reshape(nsamples, [batch_size, 1]),
        'nchannels': tf.reshape(nchannels, [batch_size, 1]),
        'chan_names': tf.reshape(chan_names, [batch_size, -1]),
        'vectorized_data': tf.reshape(vectorized_data, [batch_size, -1]),
        'psd_features': tf.reshape(psd_features, [batch_size, -1]),
        'behavioral_state': tf.reshape(behavioral_state, [batch_size, 1]),
        'pathological': tf.reshape(pathological, [batch_size, 1])
    }

    return batch_input_dict