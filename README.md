## An EEG Preprocessing Pipeline

### The library is written in Python 3.8. The following libraries are required.

1. MNE Python
2. PyMEF
3. Pandas
4. Numpy
5. Other standard python libraries
