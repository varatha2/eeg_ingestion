from __future__ import print_function

from glob import glob
import re
from scipy.io import savemat
import pandas as pd
import math
from labeling.sleep_staging import *
from utils.read_xltek_hdr import return_header

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *

_eeg_channels = ['fp1', 'f3', 'f7', 'f9', 'c3', 't7', 't9', 'tp11', 'p3', 'p7', 'p9', 'o1',
                 'fp2', 'f4', 'f8', 'f10', 'c4', 't8', 't10', 'tp12', 'tp9', 'tp10', 'p4', 'p8', 'p10', 'o2',
                 'fpz', 'fz', 'cz', 'pz', 'oz']

# _eeg_channels = ['fp1', 'f3', 'f7', 'c3', 't7', 'p3', 'p7', 'o1',
#                  'fp2', 'f4', 'f8', 'c4', 't8', 'p4', 'p8', 'o2',
#                  'fpz', 'fz', 'cz', 'pz']

yasa_behavioral_state_mapping = {'NA': -1, 'W': 0, 'N1': 1, 'N2': 2, 'N3': 3, 'R': 4}

_FREQ_BANDS = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25]}

_COH_UPPER_FREQS = list(np.arange(1.5, 31))
_COH_LOWER_FREQS = list(np.arange(0.5, 30))


input_csv = '/Volumes/m145916/Projects/CCF_Nomogram/EEG/marcia/EEG temporal/normal EEG_19Jan23.csv'
eeg_path = '/Volumes/m145916/Projects/CCF_Nomogram/EEG/marcia/TLE_Day1_EMU/'
stats_out_path = '/Volumes/eplab/Projects/TLE_Outcome/stats/ccf/12_30_wake/'


def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)

def main():
    """Test module functions."""

    os.system("clear")

    meta_data = pd.read_csv(input_csv)

    all_eeg_paths = glob(eeg_path + '*.lay')

    # process files
    for index, row in meta_data.iterrows():

        lay_name = row['Study ID'].replace('-', '_')#.replace('1539', '1593')
        lay_path = [s for s in all_eeg_paths if lay_name.split('_')[-1] in s]
        if not lay_path:
            print('Could not find path for %s' % lay_name)
            continue
        out_prefix = lay_name
        age_eeg = row['age EEG']
        szfree = 1 if row['Year 1 Outcome'] == 'Yes' else 0
        szside = 'L' if row['Surgery side'] == 'Left' else 'R'
        followup = row['duration of follow up (months)']
        learning = 1 if row['Learning'] > 85 else 0 if row['Learning'] <= 85 else float('nan')
        recall = 1 if row['Recall'] > 85 else 0 if row['Recall'] <= 85 else float('nan')
        depressed = 0 if row['BDI'] < 12 else 1 if row['BDI'] >= 12 else float('nan')
        print(row['Study ID'] + ': ' + str(age_eeg) + ' ' + str(szfree)  + ' ' + str(szside) + ' ' + str(followup) +
              ' ' + str(learning)  + ' ' + str(recall) + ' ' + str(depressed))

        try:
            # good_epochs, event_list, sleep_stage_list = standardize_eeg(lay_path[0], avg_ref=False,
            #                                                             epoch_len=3, reject_bad=True,
            #                                                             remove_artifacts=False, score_sleep=True,
            #                                                             common_ref=True, ref_ch=['cz'])

            # good_epochs, event_list = standardize_eeg(lay_path[0], avg_ref=False, epoch_len=10,
            #                                           reject_bad=True, epoch_rule='eyes closed', bandpass=(0.5,30),
            #                                           remove_artifacts=True, score_sleep=False)
            good_epochs, event_list, sleep_stage_list = standardize_eeg(lay_path[0], avg_ref=False,
                                                                        epoch_len=10, reject_bad=True,
                                                                        bandpass=(1,30),
                                                                        remove_artifacts=True, score_sleep=True)
        except Exception as e:
            print(e)
            print('Exception while processing %s.' % lay_name)
            continue

        if good_epochs:
            print(len(good_epochs))

            eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.split('-')[0].lower() in _eeg_channels]

            ec_epochs = find_wake_epochs_alpha_pow(good_epochs, eeg_ch_names, sleep_stage_list, event_list,
                                                   min_epochs=4, max_epochs=6,
                                                   remove_on_entropy=True, full_record=False)

            if len(ec_epochs) == 0:
                print('No EC epochs found in %s.' % lay_name)
                continue

            # ec_epochs = create_double_banana_montage(ec_epochs)
            #
            # if not ec_epochs:
            #     print('Double Banana Conversion Failed for %s.' % lay_name)
            #     continue

            # mne.set_eeg_reference(ec_epochs, ref_channels='average', copy=False, projection=False, ch_type='eeg')

            # ec_epochs = mne.concatenate_epochs(
            #     [remove_artifacts_ICA(ec_epochs[i], 15, muscle_low_freq=15) for i in range(len(ec_epochs))])

            ec_epochs = ec_epochs.pick(picks=eeg_ch_names)

            mne.set_eeg_reference(ec_epochs, ref_channels='average', copy=False, projection=False, ch_type='eeg')

            # good_epochs = mne.concatenate_epochs(
            #     [remove_artifacts_ICA(good_epochs[i], 15) for i in range(len(good_epochs))])
            #
            # ec_epochs = good_epochs.pick(picks=eeg_ch_names)  # pick only EEG data

            # print(eeg_ch_names)
            #
            # print(sleep_stage_list[::3])
            #
            # yasa_sleep_stages = np.vectorize(yasa_behavioral_state_mapping.get)(sleep_stage_list)
            #
            # if len(eeg_epochs) > 30:
            #     wake_epochs = eeg_epochs[yasa_sleep_stages == 0]
            # else:
            #     wake_epochs = eeg_epochs

            if len(ec_epochs) > 0:
                # average reference
                # mne.set_eeg_reference(wake_epochs, ref_channels='average', copy=False, projection=False)

                tot_power, pib, psds = eeg_power_band(ec_epochs, relative=False, freq_bands=_FREQ_BANDS,
                                                      freq_range=(1., 30), spectrum=True)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                print(pib.shape)

                tot_power, pib_relative = eeg_power_band(ec_epochs, freq_bands=_FREQ_BANDS, freq_range=(2., 25))
                pib_relative = np.swapaxes(pib_relative, 0, 1)  # epochs are in axis 1 --> move to axis 0

                coh_spectrum = calc_connectivity(ec_epochs, method=['coh'], average=False,
                                                 low_freq_range=_COH_LOWER_FREQS, high_freq_range=_COH_UPPER_FREQS)
                print(coh_spectrum.shape)

                con = calc_connectivity(ec_epochs, method=['coh', 'imcoh', 'pli'], freq_bands=_FREQ_BANDS,
                                        average=False)
                print(con.shape)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                eeg_data = ec_epochs.get_data()

                np.savez(os.path.join(stats_out_path, out_prefix), ch_names=ec_epochs.ch_names, raw=eeg_data,
                         num_wake_epochs=len(ec_epochs), pib=pib, pib_relative=pib_relative, coh=coh, imcoh=imcoh,
                         pli=pli, coh_spectrum=coh_spectrum, outcome=szfree, fup=followup, lateralization=szside,
                         age=age_eeg, learning=learning, recall=recall, depressed=depressed, psd=psds)

                save_dic = {'ch_names': ec_epochs.ch_names, 'num_wake_epochs': len(ec_epochs), 'pib': pib,
                            'pib_relative': pib_relative, 'coh': coh, 'imcoh': imcoh, 'pli': pli, 'coh_spectrum': coh_spectrum,
                            'outcome': szfree, 'fup': followup, 'lateralization': szside, 'raw': eeg_data,
                            'age': age_eeg, 'learning': learning, 'recall': recall, 'depressed': depressed, 'psd': psds}

                # np.savez(os.path.join(stats_out_path, out_prefix), ch_names = saved_channels,
                #          num_ec_epochs=len(eeg_epochs), pib=pib, con=con)
                savemat(os.path.join(stats_out_path, out_prefix + '.mat'), save_dic)

if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()