from __future__ import print_function

from glob import glob
import numpy as np
import pandas as pd
from datetime import datetime
import os
import re
import sys
import mne

from concurrent.futures import ProcessPoolExecutor
from utils.handy_functions import list_contains_substr
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *
import mne
from feature_extraction.source_localization import SourceLocalization

# xltek_path = '/Volumes/eplab/Projects/Yoga_EEG_AI/auto_eeg_process_test/sample_xltek/'
# xltek_path = '/Volumes/m145916/Projects/Brain_Health/Mayo/EEG/phase2/xltek/'
# mef_out_path = '/Volumes/eplab/Projects/Yoga_EEG_AI/auto_eeg_process_test/mef_output/'
# stats_out_path = '/Volumes/eplab/Projects/Yoga_EEG_AI/auto_eeg_process_test/stats/'
xltek_path = '/Volumes/eplab/Projects/tauPET_epilepsy/xltek/'
mef_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/mef_out/'
stats_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/stats/'
epochs_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/epochs/'
num_processes = 25

localization_method = 'eLORETA'
epoch_len = 30.
_eeg_channels = ['fp1', 'f3', 'f7', 'f9', 'c3', 't7', 't9', 'tp11', 'p3', 'p7', 'p9', 'o1',
                 'fp2', 'f4', 'f8', 'f10', 'c4', 't8', 't10', 'tp12', 'tp9', 'tp10', 'p4', 'p8', 'p10', 'o2',
                 'fpz', 'fz', 'cz', 'pz', 'oz']
_freq_bands = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25],
               "gamma": [25, 40]}

behavioral_state_numeric_mapping = {'NA': -1, 'W': 0, 'N1': 1, 'N2': 2, 'N3': 3, 'R': 4}


def _process_eeg_files_batch(process_index, ranges, output_dir, record_names):
    """Processes and saves list of EEGs as TFRecord in 1 thread.
    Args:
    process_index: integer, unique batch to run index is within [0, len(ranges)).
    ranges: list of pairs of integers specifying ranges of each batches to
      analyze in parallel.
    output_dir: where outputs will be saved
    record_names: list of strings; each string is a path to an EEG file
    """

    files_in_process = np.arange(ranges[process_index][0],
                               ranges[process_index][1], 1, dtype=int)

    # initialize source localization
    sl = SourceLocalization(freq_bands=_freq_bands, epoch_len=epoch_len)

    for i in files_in_process:
        record_name = record_names[i]
        record_base_name = os.path.basename(record_name)
        out_prefix = re.sub(r'\W+', '', record_base_name)
        mef_out_name = os.path.join(mef_out_path, out_prefix).replace(" ", "\\ ")

        record_name = record_name.replace(" ", "\\ ")

        if (not os.path.isdir(mef_out_name + '.mefd/')) or os.path.isfile(mef_out_name + '.mefd/conversion_failed.txt'):

            command = '/home/yoga/projects/scalp_cnn_framework/xltek2mef/xltek2mef3' + ' ' + record_name + '/ ' + \
                      '-noprompt' + ' ' + '-o' + ' ' + mef_out_name
            return_code = os.system(command)

            if return_code != 0:
                continue

            print('Conversion completed..')

        else:
            print("MEF directory already exists.. proceeding with stats.")

        try:
            good_epochs, event_list, sleep_stage_list = standardize_eeg(mef_out_name + '.mefd/', avg_ref=False,
                                                                        epoch_len=epoch_len, reject_bad=True,
                                                                        remove_artifacts=True, score_sleep=True)
        except Exception as e:
            print(e)
            print('Exception while processing %s.' % record_base_name)
            continue

        if good_epochs:
            print(good_epochs.info)

            eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.split('-')[0].lower() in _eeg_channels]

            print(sleep_stage_list[::3])

            sleep_stages_numeric = np.vectorize(behavioral_state_numeric_mapping.get)(sleep_stage_list)

            eeg_epochs = good_epochs.copy().pick(picks=eeg_ch_names)  # pick only EEG data

            # wake_epoch_ids = yasa_sleep_stages == 0
            all_wake_epochs = eeg_epochs[sleep_stages_numeric == 0]
            wake_epochs = all_wake_epochs[:min(len(all_wake_epochs), 20)]

            new_names = dict(
                (ch_name,
                 ch_name.rstrip('.').upper().replace('Z', 'z').replace('FP', 'Fp').replace('TP11', 'TP9').replace(
                     'TP12', 'TP10'))
                for ch_name in wake_epochs.ch_names)
            wake_epochs.rename_channels(new_names)

            # Read and set the EEG electrode locations
            montage = mne.channels.make_standard_montage('standard_1020')
            # montage.ch_names
            wake_epochs.set_montage(montage)
            wake_epochs.set_eeg_reference(projection=True)  # needed for inverse modeling

            print("Wake epochs found: " + str(len(wake_epochs)))

            if len(wake_epochs) > 0:

                stcs = sl.compute_source_estimate_epochs(wake_epochs, method=localization_method)

                # save STCs as CSV
                stc_dfs = []
                for band in stcs:
                    stc_dfs.append(stcs[band].to_data_frame())

                index_mapping = {i: k for i, (k, v) in enumerate(_freq_bands.items())}
                stc_df = pd.concat(stc_dfs, ignore_index=True).rename(index=index_mapping)
                stc_df.to_csv(os.path.join(output_dir, out_prefix + '_stc_psd_%s.csv' % localization_method))

                print('Saving source images..')

                for key in stcs.keys():
                    sl.export_nifti_image(stcs[key], os.path.join(output_dir, out_prefix +
                                                                  '_%s_%s.nii.gz' % (key, localization_method)))

                # # apply avg reference (computed earlier)
                # wake_epochs.apply_proj()
                #
                # tot_power, pib = eeg_power_band(wake_epochs)
                # pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                # print(pib.shape)
                #
                # con = calc_connectivity(wake_epochs, method=['coh', 'imcoh', 'pli'], average=False)
                # print(con.shape)
                #
                # coh = con[:, 0, :, :, :]
                # imcoh = con[:, 1, :, :, :]
                # pli = con[:, 2, :, :, :]
                #
                # all_ec_epochs = mne.concatenate_epochs([wake_epochs[i] for i in range(len(wake_epochs))], add_offset=False)
                # split_data = np.vsplit(all_ec_epochs.get_data(), len(all_ec_epochs))
                # full_data = np.squeeze(np.dstack(split_data), axis=0)
                # gfp, state_maps, gfp_per_state, p_hat, T_hat, gev, gev_sum = calc_microstate_stats(full_data,
                #                                                                                    wake_epochs.info[
                #                                                                                        'sfreq'],
                #                                                                                    wake_epochs.info[
                #                                                                                        'ch_names'])
                #
                # np.savez(os.path.join(stats_out_path, out_prefix), ch_names=wake_epochs.ch_names,
                #          num_ec_epochs=len(wake_epochs), pib=pib, coh=coh, imcoh=imcoh, pli=pli, gfp=gfp,
                #          ms_maps=state_maps, gfp_per_state=gfp_per_state, p_hat=p_hat, t_hat=T_hat,
                #          gev=gev, gev_sum=gev_sum)
        sl.reset()

def _process_eeg_files(output_dir, record_names, num_processes):
    """Process and save list of EEG records as TFRecord of Example protos.
    Args:
    output_dir: where outputs will be saved
    record_names: list of strings; each string is a path to an EEG record
    num_processes: integer number of processes to preprocess the EEGs.
    """

    # Break all images into batches with a [ranges[i][0], ranges[i][1]].
    spacing = np.linspace(0, len(record_names), num_processes + 1).astype(np.int)
    ranges = []
    for i in range(len(spacing) - 1):
        ranges.append([spacing[i], spacing[i+1]])

    # Launch a thread for each batch.
    print('Launching %d processes for spacings: %s' % (num_processes, ranges))
    sys.stdout.flush()

    with ProcessPoolExecutor(max_workers=num_processes) as executor:
        for process_index in range(len(ranges)):
            # args = (process_index, ranges, outfile_name, record_names, input_csv, num_shards)
            executor.submit(_process_eeg_files_batch, process_index, ranges, output_dir, record_names)

    print('%s: Finished writing all %d records in data set.' %
          (datetime.now(), len(record_names)))
    sys.stdout.flush()


def main():

    all_files = glob(os.path.join(xltek_path, '*'))
    _process_eeg_files(stats_out_path, all_files, num_processes)

if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()
