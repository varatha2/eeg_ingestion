'''
Test pipeline.
'''

from __future__ import print_function

import pandas as pd
from glob import glob
import re
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *
import mne
from nilearn import plotting
from nilearn import datasets
from nilearn.input_data import NiftiLabelsMasker
from matplotlib import pyplot as plt
from feature_extraction.source_localization import SourceLocalization
from labeling.sleep_staging import find_eyes_closed_epochs_alpha_peak

# xltek_path = '/Volumes/eplab/Projects/Yoga_EEG_AI/auto_eeg_process_test/sample_xltek/'
# xltek_path = '/Volumes/m145916/Projects/Brain_Health/Mayo/EEG/phase2/xltek/'
# mef_out_path = '/Volumes/eplab/Projects/Yoga_EEG_AI/auto_eeg_process_test/mef_output/'
# stats_out_path = '/Volumes/eplab/Projects/Yoga_EEG_AI/auto_eeg_process_test/stats/'
# xltek_path = '/Volumes/m145916/Projects/tauPET_epilepsy/xltek/'
# mef_out_path = '/Volumes/m145916/Projects/tauPET_epilepsy/mef_out/'
# stats_out_path = '/Volumes/m145916/Projects/tauPET_epilepsy/stats/'
# epochs_out_path = '/Volumes/m145916/Projects/tauPET_epilepsy/epochs/'

xltek_path = '/Volumes/eplab/Projects/tauPET_epilepsy/xltek/'
mef_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/mef_out/'
stats_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/stats_6_29_auto_abs/'
image_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/pdf_images/'
epochs_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/epochs/'

# ## PNES
# xltek_path = '/Volumes/m145916/Projects/CCF_Nomogram/EEG/PNES_Clean/'
# stats_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/stats_pnes_abs/'
# image_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/pdf_images_pnes/'
# mef_out_path = '/Volumes/eplab/Projects/TLE_Outcome/PNES/'

localization_method = 'eLORETA'
epoch_len = 10.
# _eeg_channels = ['fp1', 'f3', 'f7', 'f9', 'c3', 't7', 't9', 'tp11', 'p3', 'p7', 'p9', 'o1',
#                  'fp2', 'f4', 'f8', 'f10', 'c4', 't8', 't10', 'tp12', 'tp9', 'tp10', 'p4', 'p8', 'p10', 'o2',
#                  'fpz', 'fz', 'cz', 'pz', 'oz']

_eeg_channels = ['fp1', 'f3', 'f7', 'c3', 't7', 'p3', 'p7', 'o1',
                 'fp2', 'f4', 'f8', 'c4', 't8', 'p4', 'p8', 'o2',
                 'fpz', 'fz', 'cz', 'pz']

_freq_bands = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25],
               "gamma": [25, 40]}

behavioral_state_numeric_mapping = {'NA': -1, 'W': 0, 'N1': 1, 'N2': 2, 'N3': 3, 'R': 4}


def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)

def main():
    """Test module functions."""

    os.system("clear")

    all_xltek = sorted(glob(os.path.join(xltek_path, '*')))

    # initialize source localization
    sl = SourceLocalization(freq_bands=_freq_bands, epoch_len=epoch_len)

    # fetch AAL atlas
    dataset = datasets.fetch_atlas_aal()
    atlas_filename = dataset.maps
    labels = dataset.labels
    masker = NiftiLabelsMasker(labels_img=atlas_filename, standardize=False, strategy='median')

    # process files
    for curr_xltek in all_xltek:
        record_name = os.path.basename(curr_xltek)
        out_prefix = re.sub(r'\W+', '', record_name)
        mef_out_name = os.path.join(mef_out_path, out_prefix).replace(" ", "\\ ")

        curr_xltek = curr_xltek.replace(" ", "\\ ")

        if not os.path.isdir(mef_out_name + '.mefd/'):

            command = './xltek2mef3' + ' ' + curr_xltek + '/ ' + '-noprompt' + ' ' + '-o' + ' ' + mef_out_name
            return_code  = os.system(command)

            if return_code != 0:
                continue

            print('Conversion completed..')

        else:
            print("MEF directory already exists.. proceeding with stats.")

        try:
            good_epochs, event_list, sleep_stage_list = standardize_eeg(mef_out_name + '.mefd/', avg_ref=False,
                                                                        epoch_len=epoch_len, reject_bad=True,
                                                                        remove_artifacts=False, score_sleep=True)
        except Exception as e:
            print(e)
            print('Exception while processing %s.' % record_name)
            continue

        if good_epochs:
            print(record_name)
            # print(good_epochs.info)

            eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.split('-')[0].lower() in _eeg_channels]

            # print(sleep_stage_list[::3])
            #
            # sleep_stages_numeric = np.vectorize(behavioral_state_numeric_mapping.get)(sleep_stage_list)
            # wake_epoch_ids = yasa_sleep_stages == 0
            # all_wake_epochs = eeg_epochs[sleep_stages_numeric == 0]
            # wake_epochs = all_wake_epochs[:min(len(all_wake_epochs), 10)]

            ec_epochs = find_eyes_closed_epochs_alpha_peak(good_epochs, eeg_ch_names, sleep_stage_list, event_list, 6)

            if len(ec_epochs) == 0:
                print('No EC epochs found.')
                continue

            ec_epochs = mne.concatenate_epochs([remove_artifacts_ICA(ec_epochs[i], 15) for i in range(len(ec_epochs))])

            ec_epochs = ec_epochs.copy().pick(picks=eeg_ch_names)#.resample(100.)  # pick only EEG data

            # front_channels = [ch for ch in ec_epochs.ch_names if 'f' in ch.lower()]
            # ec_epochs = ec_epochs.resample(100.).filter(2.0, 13.0, picks=front_channels)

            print("EC epochs found: " + str(len(ec_epochs)))

            if len(ec_epochs) > 0:

                new_names = dict(
                    (ch_name,
                     ch_name.rstrip('.').upper().replace('Z', 'z').replace('FP', 'Fp').replace('TP11', 'TP9').replace(
                         'TP12', 'TP10'))
                    for ch_name in ec_epochs.ch_names)
                ec_epochs.rename_channels(new_names)

                # Read and set the EEG electrode locations
                montage = mne.channels.make_standard_montage('standard_1020')
                # montage.ch_names
                ec_epochs.set_montage(montage)
                ec_epochs.set_eeg_reference(projection=True)  # needed for inverse modeling

                stcs = sl.compute_source_estimate_epochs(ec_epochs, method=localization_method, relative=False, n_jobs=16)

                # save STCs as CSV
                stc_dfs = []
                for band in stcs:
                    stc_dfs.append(stcs[band].to_data_frame())

                index_mapping = {i: k for i, (k, v) in enumerate(_freq_bands.items())}
                stc_df = pd.concat(stc_dfs, ignore_index=True).rename(index=index_mapping)
                stc_df.to_csv(os.path.join(stats_out_path, out_prefix + '_stc_psd_%s.csv' % localization_method))

                print('Saving source images..')

                nii_names = []

                for i, key in enumerate(stcs.keys()):
                    nii_name = os.path.join(stats_out_path, out_prefix + '_%s_%s.nii.gz' % (key, localization_method))
                    nii_names.append(nii_name)
                    sl.export_nifti_image(stcs[key], nii_name)

                time_series = masker.fit_transform(nii_names)
                aal_df = pd.DataFrame(time_series, columns=labels, index=stcs.keys())
                aal_df.to_csv(os.path.join(stats_out_path, out_prefix + '_aal_rois_%s.csv' % localization_method))

                # apply avg reference (computed earlier)
                ec_epochs.apply_proj()

                tot_power, pib, psds = eeg_power_band(ec_epochs, relative=False, freq_bands=_freq_bands,
                                                      spectrum=True)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                print(pib.shape)
                print(psds.shape)

                con = calc_connectivity(ec_epochs, method=['coh', 'imcoh', 'pli'], freq_bands=_freq_bands,
                                        average=False)
                print(con.shape)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                np.savez(os.path.join(stats_out_path, out_prefix), ch_names=ec_epochs.ch_names,
                         num_ec_epochs=len(ec_epochs), pib=pib, coh=coh, imcoh=imcoh, pli=pli, psd=psds)

        sl.reset()

        # delete mef
        # shutil.rmtree(mef_out_name + '.mefd/')


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()