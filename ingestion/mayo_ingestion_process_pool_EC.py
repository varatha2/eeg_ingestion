'''
Python script to load and ingest EEG records as tensorflow examples.

Mini example:
Full example:
python3 -m ingestion.mayo_ingestion_process_pool_EC \
--data_dir='/home/yoga/data/mef_data/test_mef_corpus/' \
--outfile_name='/home/yoga/data/mef_data/tfrecords/mayo_100_4_20' \
--input_csv='/home/yoga/data/mef_data/downloadable_list.csv' \
--epoch_len=30.0 \
--num_processes=20 \
--num_shards=20


Full example:
python3 -m ingestion.mayo_ingestion_process_pool_EC \
--data_dir='/mnt/Hydrogen/yoga/mef_data/' \
--stats_out_path='/Volumes/eplab/Projects/Yoga_EEG_AI/ec_stats_7_19/' \
--input_csv='/mnt/Hydrogen/yoga/downloadable_list.csv' \
--epoch_len=10.0 \
--num_processes=48 \
--num_shards=240
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import glob
import numpy as np
import pandas as pd
from datetime import datetime
import os
import re
import sys
import mne
from absl import app, flags

from concurrent.futures import ProcessPoolExecutor
from utils.handy_functions import list_contains_substr
from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *
from nilearn import datasets, image
from nilearn.input_data import NiftiLabelsMasker
import nibabel
from feature_extraction.source_localization import SourceLocalization
from labeling.sleep_staging import *

import os
num_cpu = '1' # Set as a string (for linear algebra functions to use only one CPU per process)
os.environ['OMP_NUM_THREADS'] = num_cpu

FLAGS = flags.FLAGS

flags.DEFINE_string('data_dir', '', 'Absolute path to the directory where images are located')
flags.DEFINE_string('stats_out_path', '', 'Output directory including absolute path')
flags.DEFINE_string('input_csv', '', 'Absolute path to the CSV file containing labels')
flags.DEFINE_float('epoch_len', 10.0, 'Length of EEG epochs.')
flags.DEFINE_string('reconstruction_method', 'eLORETA', 'Source reconstruction method')
flags.DEFINE_integer('num_shards', 1, 'Number of shards in TFRecord files.')
flags.DEFINE_integer('num_processes', 1, 'Number of processes to preprocess the records.')

# EEG channels being used
_eeg_channels = ['fp1', 'f3', 'f7', 'c3', 't7', 'p3', 'p7', 'o1',
                 'fp2', 'f4', 'f8', 'c4', 't8', 'p4', 'p8', 'o2',
                 'fpz', 'fz', 'cz', 'pz']

_freq_bands = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25],
               "gamma": [25, 40]}

_COH_UPPER_FREQS = list(np.arange(1.5, 46))
_COH_LOWER_FREQS = list(np.arange(0.5, 45))


def _process_eeg_files_batch(process_index, ranges, record_names, input_csv):
    """Processes and saves list of EEGs as TFRecord in 1 thread.
    Args:
    thread_index: integer, unique batch to run index is within [0, len(ranges)).
    ranges: list of pairs of integers specifying ranges of each batches to
      analyze in parallel.
    record_names: list of strings; each string is a path to an EEG file
    input_csv: name of the CSV file containing label information.
    """

    num_files_in_thread = ranges[process_index][1] - ranges[process_index][0]

    counter = 0

    if not (os.path.isfile(input_csv)):
        return

    label_csv = pd.read_csv(input_csv)
    unique_csv = label_csv.drop_duplicates(subset="Directory", keep="last")

    # # initialize source localization
    # sl = SourceLocalization(freq_bands=_freq_bands, epoch_len=FLAGS.epoch_len, vol_source_pos=10.0)
    #
    # # fetch AAL atlas
    # dataset = datasets.fetch_atlas_aal()
    # atlas_filename = dataset.maps
    # roi_labels = dataset.labels
    # masker = NiftiLabelsMasker(labels_img=atlas_filename, standardize=False, strategy='median')

    shard_counter = 0
    files_in_shard = np.arange(ranges[process_index][0], ranges[process_index][1], dtype=int)
    for i in files_in_shard:
        record_name = record_names[i]

        # Find patient id and EEG date from EEG name.
        curr_file = os.path.basename(record_name)

        # check for already processed files
        # if os.path.isfile(os.path.join(FLAGS.stats_out_path, curr_file[:-5] + '.npz')):
        #     npz_file = np.load(os.path.join(FLAGS.stats_out_path, curr_file[:-5] + '.npz'))
        #     if 'raw' in npz_file:
        #         print('File already processed: %s.' % record_name)
        #         shard_counter += 1
        #         counter += 1
        #         continue

        parts = curr_file.split('_')
        mrn = parts[0]
        eeg_date = parts[1][:-5]
        eeg_dt = datetime.strptime(eeg_date, '%Y%m%dT%H%M%S')

        curr_eeg_meta_data = unique_csv[
            (pd.to_datetime(unique_csv["Date"]).dt.date == eeg_dt.date()) & (unique_csv["MRN"] == int(mrn))]

        if len(curr_eeg_meta_data) > 0:
            pathological = curr_eeg_meta_data.Label.values[0]
            if curr_eeg_meta_data.Label.values[0] == 10:
                print('PNES example: %s,' % record_name)
                pathological = 5
            # age = (eeg_dt.year - pd.to_datetime(curr_eeg_meta_data.MFDOB).dt.year).values[0]
            age = curr_eeg_meta_data.Age_EEG.values[0]
            complaint = curr_eeg_meta_data.Complaint.values[0]

            try:
                good_epochs, event_list, sleep_stage_list = standardize_eeg(record_name, avg_ref=False,
                                                                            remove_artifacts=False,
                                                                            epoch_len=FLAGS.epoch_len,
                                                                            reject_bad=True, score_sleep=True)
            except:
                print('Exception while processing %s.' % record_name)
                continue

            if good_epochs:

                eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.rstrip('-').lower() in _eeg_channels]

                if len(eeg_ch_names) != 20:
                    print('Missing channels in %s.' % record_name)
                    print(eeg_ch_names)
                    continue

                # ec_epochs = find_eyes_closed_epochs_alpha_peak(good_epochs, eeg_ch_names, sleep_stage_list, event_list, 6)
                ec_epochs = find_eyes_closed_epochs_alpha_pow(good_epochs, eeg_ch_names, sleep_stage_list, event_list,
                                                               max_epochs=6, min_epochs=1)

                if len(ec_epochs) == 0:
                    print('No EC epochs found in %s.' % record_name)
                    continue

                # ec_epochs = mne.concatenate_epochs(
                #     [remove_artifacts_ICA(remove_artifacts_SSP(ec_epochs[i]), 15) for i in range(len(ec_epochs))])

                ec_epochs = mne.concatenate_epochs(
                    [remove_artifacts_ICA(ec_epochs[i], 15, foof_freq_range=(1, 45), alpha_range=(6, 13),
                         muscle_freq_range=(14, 45)) for i in range(len(ec_epochs))])

                ec_epochs = ec_epochs.copy().reorder_channels(ch_names=_eeg_channels)

                new_names = dict(
                    (ch_name,
                     ch_name.rstrip('.').upper().replace('Z', 'z').replace('FP', 'Fp'))
                    for ch_name in ec_epochs.ch_names)
                ec_epochs.rename_channels(new_names)

                # Set the EEG electrode locations
                montage = mne.channels.make_standard_montage('standard_1020')
                ec_epochs.set_montage(montage)

                ec_epochs.set_eeg_reference(projection=True)

                # try:
                #     stc = sl.compute_source_estimate_epochs(ec_epochs, method=FLAGS.reconstruction_method,
                #                                              relative=False, full_spectrum=True, n_jobs=1)
                #     # nii_imgs = []
                #     #
                #     # for _, key in enumerate(stcs.keys()):
                #     #     nii_imgs.append(sl.export_nifti_image(stcs[key], '', return_image=True))
                #     #
                #     # combined_img = image.concat_imgs(nii_imgs)
                #     # nibabel.save(combined_img, os.path.join(FLAGS.stats_out_path, curr_file[:-5] + '.nii.gz'))
                #     #
                #     # time_series = masker.fit_transform(combined_img)
                #     # aal_df = pd.DataFrame(time_series, columns=roi_labels, index=stcs.keys())
                #     # aal_df.to_csv(os.path.join(FLAGS.stats_out_path, curr_file[:-5] + '_aal_rois.csv'))
                #     # del stcs, nii_imgs, aal_df, combined_img, time_series
                #
                #     nii_img = sl.export_nifti_image(stc, '', return_image=True)
                #     nibabel.save(nii_img, os.path.join(FLAGS.stats_out_path, curr_file[:-5] + '.nii.gz'))
                #     time_series = masker.fit_transform(nii_img)
                #     aal_df = pd.DataFrame(time_series, columns=roi_labels)
                #     aal_df.to_csv(os.path.join(FLAGS.stats_out_path, curr_file[:-5] + '_aal_rois.csv'))
                #
                #     del stc, nii_img, aal_df, time_series
                #
                # except Exception as e:
                #     print('Exception during source computation %s: %s.' % (record_name, str(e)))

                # apply avg reference (computed earlier)
                ec_epochs.apply_proj()

                tot_power, pib, psds = eeg_power_band(ec_epochs, relative=False,
                                                      freq_bands=_freq_bands, spectrum=True)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                topo_data = np.array([generate_topomap_data(pib_c, ec_epochs.info) for pib_c in pib])

                con = calc_connectivity(ec_epochs, method=['coh', 'imcoh', 'pli'],
                                        freq_bands=_freq_bands, average=False)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                coh_spectrum = calc_connectivity(ec_epochs, method=['coh'], average=False,
                                                 low_freq_range=_COH_LOWER_FREQS, high_freq_range=_COH_UPPER_FREQS)
                full_coh = calc_connectivity(ec_epochs, method=['coh'], average=False,
                                             low_freq_range=(.5), high_freq_range=(45.))

                eeg_data = ec_epochs.get_data()

                sfi = -1
                stden = -1
                try:
                    sfi, stden = calc_fragmentation_metrics(sleep_stage_list)
                except Exception as e:
                    print('Error calculating sleep index %s: %s.' % (record_name, str(e)))

                np.savez(os.path.join(FLAGS.stats_out_path, curr_file[:-5]),
                         subject_id=mrn,
                         age=age,
                         eeg_guid=curr_file[:-5],
                         ch_names=ec_epochs.ch_names,
                         num_ec_epochs=len(ec_epochs),
                         raw=eeg_data,
                         pib=pib,
                         coh=coh,
                         full_coh=full_coh,
                         imcoh=imcoh,
                         pli=pli,
                         psd=psds,
                         csd=coh_spectrum,
                         topomap_data=topo_data,
                         pathological=pathological,
                         sfi=sfi,
                         std=stden,
                         complaint=complaint
                         )

                del eeg_data, topo_data, pib, psds, con

                shard_counter += 1
                counter += 1

            else:
                print('Could not find good epochs in %s' % record_name)

        else:
            print('Could not locate metadata for %s' % record_name)

        if not counter % 10:
            print('%s [process %d]: Processed %d of %d records in thread batch.' %
                  (datetime.now(), process_index, counter, num_files_in_thread))
        sys.stdout.flush()

    print('%s [process %d]: Wrote %d records.' %
          (datetime.now(), process_index, counter))
    sys.stdout.flush()

    # return 'Success'


def _process_eeg_files(record_names, input_csv, num_processes):
    """Process and save list of EEG records as TFRecord of Example protos.
    Args:
    record_names: list of strings; each string is a path to an EEG record
    input_csv: name of the CSV file containing label information.
    num_processes: integer number of processes to preprocess the EEGs.
    """

    # Break all images into batches with a [ranges[i][0], ranges[i][1]].
    spacing = np.linspace(0, len(record_names), num_processes + 1).astype(np.int)
    ranges = []
    for i in range(len(spacing) - 1):
        ranges.append([spacing[i], spacing[i+1]])

    # Launch a thread for each batch.
    print('Launching %d processes for spacings: %s' % (num_processes, ranges))
    sys.stdout.flush()

    # _process_eeg_files_batch(0, ranges, record_names, input_csv)

    with ProcessPoolExecutor(max_workers=num_processes) as executor:
        for process_index in range(len(ranges)):
            # args = (process_index, ranges, outfile_name, record_names, input_csv, num_shards)
            executor.submit(_process_eeg_files_batch, process_index, ranges, record_names, input_csv)
            # try:
            #     exit_status = future.result()
            # except Exception as exc:
            #     print(repr(future.exception()))
            # else:
            #     print(f"{exit_status}: {process_index}")

    print('%s: Finished writing all %d records in data set.' %
          (datetime.now(), len(record_names)))
    sys.stdout.flush()


def main(argv):

    all_files = glob.glob(os.path.join(FLAGS.data_dir, '*.mefd'))
    _process_eeg_files(all_files, FLAGS.input_csv, FLAGS.num_processes)


if __name__ == '__main__':
    app.run(main)
