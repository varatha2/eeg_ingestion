from __future__ import print_function

from glob import glob
import pandas as pd
import re
from scipy.io import savemat

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *

_eeg_channels = ['fp1', 'f3', 'f7', 'f9', 'c3', 't7', 't9', 'tp11', 'p3', 'p7', 'p9', 'o1',
                 'fp2', 'f4', 'f8', 'f10', 'c4', 't8', 't10', 'tp12', 'tp9', 'tp10', 'p4', 'p8', 'p10', 'o2',
                 'fpz', 'fz', 'cz', 'pz', 'oz']


input_path = '/Volumes/m145916/Projects/PCA_Coma_Study/deidentified_data/'
input_csv = '/Volumes/m145916/Projects/PCA_Coma_Study/deidentified_data/meta_data.csv'
stats_out_path = '/Volumes/m145916/Projects/PCA_Coma_Study/pipeline_stats/'
epochs_out_path = '/Volumes/m145916/Projects/PCA_Coma_Study/mne_epochs/'

def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)

def main():
    """Test module functions."""

    os.system("clear")

    meta_data = pd.read_csv(input_csv)

    all_files = sorted(glob(os.path.join(input_path, '*.lay')))

    # all_files = ['/Volumes/m145916/Projects/PCA_Coma_Study/deidentified_data/ID8.lay',
    #              '/Volumes/m145916/Projects/PCA_Coma_Study/deidentified_data/ID83.lay']

    # process files
    for index, row in meta_data.iterrows():

        lay_name = str(int(row['Record ID']))
        lay_path = [s for s in all_files if 'ID'+lay_name+'.lay' in s] + \
                   [s for s in all_files if 'ID'+lay_name+'_revised.lay' in s]
        if not lay_path:
            print('Could not find path for ID %s' % lay_name)
            continue

        record_name = os.path.basename(lay_path[0])
        out_prefix = 'ID' + re.sub(r'\W+', '', record_name)

        outcome = row['Group']
        relative_eeg_time = row['First EEG time relative to arrest']
        sedative = row['first_sedatives?']

        print('ID ' + lay_name + ': ' + str(outcome) + ' ' + str(relative_eeg_time))

        try:
            good_epochs, event_list = standardize_eeg(lay_path[0], avg_ref=False, epoch_len=30,
                                                      reject_bad=False, remove_artifacts=True, score_sleep=False)
        except Exception as e:
            print(e)
            print('Exception while processing %s.' % lay_name)
            continue

        if good_epochs:
            print(len(good_epochs))

            eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.split('-')[0].lower() in _eeg_channels]

            eeg_epochs = good_epochs.pick(picks=eeg_ch_names)  # pick only EEG data

            if len(eeg_epochs) > 0:
                # average reference
                # mne.set_eeg_reference(wake_epochs, ref_channels='average', copy=False, projection=False)

                tot_power, pib = eeg_power_band(eeg_epochs, relative=False)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                print(pib.shape)

                tot_power, pib_relative = eeg_power_band(eeg_epochs)
                pib_relative = np.swapaxes(pib_relative, 0, 1)  # epochs are in axis 1 --> move to axis 0

                con = calc_connectivity(eeg_epochs, method=['coh', 'imcoh', 'pli'], average=False)
                print(con.shape)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                np.savez(os.path.join(stats_out_path, out_prefix), ch_names=eeg_epochs.ch_names,
                         num_wake_epochs=len(eeg_epochs), pib=pib, pib_relative=pib_relative, coh=coh, imcoh=imcoh,
                         pli=pli, outcome=outcome, relative_eeg_time=relative_eeg_time, sedative=sedative)


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()