'''
Test pipeline.
'''

from __future__ import print_function

import pandas as pd
from glob import glob
import re
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *
import mne
from nilearn import plotting
from matplotlib import pyplot as plt
from feature_extraction.source_localization import SourceLocalization


# xltek_path = '/Volumes/m145916/Projects/tauPET_epilepsy/xltek/'
# mef_out_path = '/Volumes/m145916/Projects/tauPET_epilepsy/mef_out/'
# stats_out_path = '/Volumes/m145916/Projects/tauPET_epilepsy/stats_whole_brain/'
# image_out_path = '/Volumes/m145916/Projects/tauPET_epilepsy/pdf_images/'
# epochs_out_path = '/Volumes/m145916/Projects/tauPET_epilepsy/epochs/'

xltek_path = '/Volumes/eplab/Projects/tauPET_epilepsy/xltek/'
mef_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/mef_out/'
stats_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/stats_EC_with_bad_epoch_removal/'
image_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/pdf_images_EC_unprocessed/'
epochs_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/epochs/'

# xltek_path = '/Volumes/m145916/Projects/post_surgical_eegs/xltek/'
# mef_out_path = '/Volumes/m145916/Projects/post_surgical_eegs/mef_out/'
# stats_out_path = '/Volumes/m145916/Projects/post_surgical_eegs/stats/'
# image_out_path = '/Volumes/m145916/Projects/post_surgical_eegs/pdf_images/'

localization_method = 'eLORETA'
epoch_len = 10.
_eeg_channels = ['fp1', 'f3', 'f7', 'c3', 't7', 'p3', 'p7', 'o1',
                 'fp2', 'f4', 'f8', 'c4', 't8', 'p4', 'p8', 'o2',
                 'fpz', 'fz', 'cz', 'pz']
_freq_bands = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25],
               "gamma": [25, 40]}


def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)

def main():
    """Test module functions."""

    os.system("clear")

    all_xltek = sorted(glob(os.path.join(xltek_path, '*')))

    # initialize source localization
    sl = SourceLocalization(freq_bands=_freq_bands, epoch_len=epoch_len)
    # sl = SourceLocalization(freq_bands=_freq_bands, epoch_len=epoch_len,
    #                         vol_labels=['Left-Cerebral-Cortex', 'Right-Cerebral-Cortex'])

    # process files
    for curr_xltek in all_xltek:
        record_name = os.path.basename(curr_xltek)
        out_prefix = re.sub(r'\W+', '', record_name)
        mef_out_name = os.path.join(mef_out_path, out_prefix).replace(" ", "\\ ")

        curr_xltek = curr_xltek.replace(" ", "\\ ")

        if not os.path.isdir(mef_out_name + '.mefd/'):

            command = '/Volumes/eplab/Local_Apps/xltek2mef3' + ' ' + curr_xltek + '/ ' + '-noprompt' + ' ' + '-o' + ' ' + mef_out_name
            return_code  = os.system(command)

            if return_code != 0:
                continue

            print('Conversion completed..')

        else:
            print("MEF directory already exists.. proceeding with stats.")

        try:
            ec_epochs, event_list = standardize_eeg(mef_out_name + '.mefd/', avg_ref=False, epoch_len=10,
                                                    epoch_rule='eyes closed', reject_bad=True, remove_artifacts=False)
        except Exception as e:
            print(e)
            print('Exception while processing %s.' % record_name)
            continue

        if ec_epochs:
            print(record_name)
            print(ec_epochs.info)

            eeg_ch_names = [ch for ch in ec_epochs.ch_names if ch.split('-')[0].lower() in _eeg_channels]

            valid = []
            for i in range(len(ec_epochs)):
                epoch_data = np.squeeze(ec_epochs[i].get_data(picks=eeg_ch_names), axis=0)
                ent_vals = entropy(epoch_data)
                valid.append(1 if np.sum(ent_vals < 0.4) == 0 else 0)

            ec_epochs = ec_epochs[np.array(valid) == 1]

            if len(ec_epochs) == 0:
                print('Could not find clean segments..')
                continue

            blink_counts, thresh = count_eye_blinks(ec_epochs, threshold=0.0003)

            ec_epochs = ec_epochs[blink_counts == 0]

            if len(ec_epochs) == 0:
                print('No EC epochs found.')
                continue

            psds, freqs = psd_welch(ec_epochs, fmin=8., fmax=13.)
            tot_alpha_power = np.sum(psds, axis=-1, keepdims=False)
            alpha_occ = np.reshape(tot_alpha_power[:, ec_epochs.info['ch_names'].index('o1')] \
                                   + tot_alpha_power[:, ec_epochs.info['ch_names'].index('o2')], [-1])

            index_array = np.argsort(alpha_occ)
            ec_epochs = ec_epochs[index_array]

            ec_epochs = ec_epochs[-6:]

            ec_epochs = mne.concatenate_epochs([remove_artifacts_ICA(ec_epochs[i], 15) for i in range(len(ec_epochs))])

            eeg_epochs = ec_epochs.copy().pick(picks=eeg_ch_names).resample(100.)  # pick only EEG data

            new_names = dict(
                (ch_name,
                 ch_name.rstrip('.').upper().replace('Z', 'z').replace('FP', 'Fp').replace('TP11', 'TP9').replace(
                     'TP12', 'TP10'))
                for ch_name in eeg_epochs.ch_names)
            eeg_epochs.rename_channels(new_names)

            # Read and set the EEG electrode locations
            montage = mne.channels.make_standard_montage('standard_1020')
            # montage.ch_names
            eeg_epochs.set_montage(montage)
            eeg_epochs.set_eeg_reference(projection=True)  # needed for inverse modeling

            print("EC epochs found: " + str(len(eeg_epochs)))

            if len(eeg_epochs) > 0:

                stcs = sl.compute_source_estimate_epochs(eeg_epochs, method=localization_method)

                # save STCs as CSV
                stc_dfs = []
                for band in stcs:
                    stc_dfs.append(stcs[band].to_data_frame())

                index_mapping = {i: k for i, (k, v) in enumerate(_freq_bands.items())}
                stc_df = pd.concat(stc_dfs, ignore_index=True).rename(index=index_mapping)
                stc_df.to_csv(os.path.join(stats_out_path, out_prefix + '_stc_psd_%s.csv' % localization_method))

                print('Saving source images..')

                fig, axs = plt.subplots(1, len(stcs.keys()), figsize=(18, 2))
                for i, key in enumerate(stcs.keys()):
                    nii_name = os.path.join(stats_out_path, out_prefix + '_%s_%s.nii.gz' % (key, localization_method))
                    sl.export_nifti_image(stcs[key], nii_name)
                    plotting.plot_img(nii_name, figure=fig, display_mode='z', cut_coords=[0],
                                              axes=axs[i], colorbar=True, annotate=False, draw_cross=False, vmin=0,
                                              vmax=30, cmap='jet')
                    # plotting.plot_glass_brain(nii_name, figure=fig, display_mode='z', cut_coords=[0], vmin=0, vmax=30,
                    #                           axes=axs[i], colorbar=True, annotate=False, draw_cross=False, cmap='jet')
                    axs[i].set_title(key)
                plt.savefig(os.path.join(image_out_path, out_prefix + '_%s.png' % localization_method), dpi=300.)

                # apply avg reference (computed earlier)
                eeg_epochs.apply_proj()

                tot_power, pib = eeg_power_band(eeg_epochs)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                print(pib.shape)

                con = calc_connectivity(eeg_epochs, method=['coh', 'imcoh', 'pli'], average=False)
                print(con.shape)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                all_ec_epochs = mne.concatenate_epochs([eeg_epochs[i] for i in range(len(eeg_epochs))], add_offset=False)
                split_data = np.vsplit(all_ec_epochs.get_data(), len(all_ec_epochs))
                full_data = np.squeeze(np.dstack(split_data), axis=0)
                gfp, state_maps, gfp_per_state, p_hat, T_hat, gev, gev_sum = calc_microstate_stats(full_data,
                                                                                                   eeg_epochs.info[
                                                                                                       'sfreq'],
                                                                                                   eeg_epochs.info[
                                                                                                       'ch_names'])

                np.savez(os.path.join(stats_out_path, out_prefix), ch_names=eeg_epochs.ch_names,
                         num_ec_epochs=len(eeg_epochs), pib=pib, coh=coh, imcoh=imcoh, pli=pli, gfp=gfp,
                         ms_maps=state_maps, gfp_per_state=gfp_per_state, p_hat=p_hat, t_hat=T_hat,
                         gev=gev, gev_sum=gev_sum)

                # ec_epochs.save(os.path.join(epochs_out_path, out_prefix + '-EC-epo.fif'), overwrite=True)

        sl.reset()

        # delete mef
        # shutil.rmtree(mef_out_name + '.mefd/')


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()