from __future__ import print_function

from glob import glob
import re
from scipy.io import savemat
import math
from labeling.sleep_staging import *
from utils.read_xltek_hdr import return_header

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *

_eeg_channels = ['fp1', 'f3', 'f7', 'f9', 'c3', 't7', 't9', 'tp11', 'p3', 'p7', 'p9', 'o1',
                 'fp2', 'f4', 'f8', 'f10', 'c4', 't8', 't10', 'tp12', 'tp9', 'tp10', 'p4', 'p8', 'p10', 'o2',
                 'fpz', 'fz', 'cz', 'pz', 'oz']

_FREQ_BANDS = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25]}

_COH_UPPER_FREQS = list(np.arange(1.5, 31))
_COH_LOWER_FREQS = list(np.arange(0.5, 30))


input_path = '/Volumes/m145916/Projects/CCF_Nomogram/EEG/PNES_Clean/'
stats_out_path = '/Volumes/eplab/Projects/TLE_Outcome/stats/pnes/11_20_ec/'
mef_out_path = '/Volumes/eplab/Projects/TLE_Outcome/PNES/'


def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)

def main():
    """Test module functions."""

    os.system("clear")

    all_xltek = sorted(glob(os.path.join(input_path, '*')))

    # process files
    for curr_xltek in all_xltek:
        record_name = os.path.basename(curr_xltek)
        out_prefix = re.sub(r'\W+', '', record_name)

        mef_out_name = os.path.join(mef_out_path, out_prefix).replace(" ", "\\ ")

        print(curr_xltek)

        xltek_hdr = return_header(curr_xltek)

        if xltek_hdr[0]['headbox_type'][0] == 20 and xltek_hdr[0]['num_channels'] > 46:
            print('Potential ECOG recording, skipping..')
            print('Headbox: ' + str(xltek_hdr[0]['headbox_type'][0]))
            print('# channels: ' + str(xltek_hdr[0]['num_channels']))
            continue

        curr_xltek = curr_xltek.replace(" ", "\\ ")

        # if os.path.isfile(os.path.join(stats_out_path, out_prefix + '.mat')):
        #     print('Already extracted features..')
        #     continue

        # Time difference between first and lst ERD files in hours.
        erd_max_diff = divmod((xltek_hdr[-1]['creation_time'] - xltek_hdr[0]['creation_time']).total_seconds(), 3600)

        if (not os.path.isdir(mef_out_name + '.mefd/')) or os.path.isfile(mef_out_name + '.mefd/conversion_failed.txt'):
            # curr_xltek = xltek_out_path + record_name
            command = '/Volumes/eplab/Local_Apps/xltek2mef3' + ' ' + curr_xltek + '/ ' + '-noprompt' + ' ' + '-o' + \
                      ' ' + mef_out_name
            # command = '/home/yoga/projects/scalp_cnn_framework/xltek2mef/xltek2mef3' + ' ' + curr_xltek + '/ ' +\
            #           '-noprompt' + ' ' + '-o' + ' ' + mef_out_name
            return_code = os.system(command)

            if return_code != 0:
                continue

            print('Conversion completed..')

        else:
            print("MEF directory already exists.. proceeding with stats.")

        try:
            good_epochs, event_list, sleep_stage_list = standardize_eeg(mef_out_name + '.mefd/', avg_ref=False,
                                                                        epoch_len=3, reject_bad=True,
                                                                        remove_artifacts=False, score_sleep=True)

            # good_epochs, event_list = standardize_eeg(mef_out_name + '.mefd/', avg_ref=False, epoch_len=10,
            #                                           reject_bad=True, epoch_rule='eyes closed', bandpass=(0.5,30),
            #                                           remove_artifacts=True, score_sleep=False)
        except Exception as e:
            print(e)
            print('Exception while processing %s.' % record_name)
            continue

        if good_epochs:
            print('# EC epochs: ' + str(len(good_epochs)))

            eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.split('-')[0].lower() in _eeg_channels]

            ec_epochs = find_wake_epochs_alpha_pow(good_epochs, eeg_ch_names, sleep_stage_list, event_list,
                                                   min_epochs=20, max_epochs=30)

            if len(ec_epochs) == 0:
                print('No EC epochs found in %s.' % mef_out_name)
                continue

            wake_epochs = ec_epochs.pick(picks=eeg_ch_names)  # pick only EEG data

            # print(sleep_stage_list[::3])
            #
            # yasa_sleep_stages = np.vectorize(yasa_behavioral_state_mapping.get)(sleep_stage_list)
            #
            # # wake_epoch_ids = yasa_sleep_stages == 0
            # wake_epochs = eeg_epochs[yasa_sleep_stages == 0]

            if len(wake_epochs) > 0:
                # average reference
                # mne.set_eeg_reference(wake_epochs, ref_channels='average', copy=False, projection=False)

                tot_power, pib, psds = eeg_power_band(wake_epochs, relative=False, freq_bands=_FREQ_BANDS,
                                                      freq_range=(1., 30), spectrum=True)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                print(pib.shape)

                tot_power, pib_relative = eeg_power_band(wake_epochs, freq_bands=_FREQ_BANDS, freq_range=(1., 30))
                pib_relative = np.swapaxes(pib_relative, 0, 1)  # epochs are in axis 1 --> move to axis 0

                coh_spectrum = calc_connectivity(wake_epochs, method=['coh'], average=False,
                                                 low_freq_range=_COH_LOWER_FREQS, high_freq_range=_COH_UPPER_FREQS)
                print(coh_spectrum.shape)

                con = calc_connectivity(wake_epochs, method=['coh', 'imcoh', 'pli'], freq_bands=_FREQ_BANDS,
                                        average=False)
                print(con.shape)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                np.savez(os.path.join(stats_out_path, out_prefix), ch_names=wake_epochs.ch_names,
                         num_wake_epochs=len(wake_epochs), pib=pib, pib_relative=pib_relative, coh=coh, imcoh=imcoh,
                         pli=pli, coh_spectrum=coh_spectrum, outcome=5, fup=0, lateralization=3,
                         age=30, duration=erd_max_diff, psd=psds)

                save_dic = {'ch_names': wake_epochs.ch_names, 'num_wake_epochs': len(wake_epochs), 'pib': pib,
                            'pib_relative': pib_relative, 'coh': coh, 'imcoh': imcoh, 'pli': pli,
                            'coh_spectrum': coh_spectrum, 'outcome': 5, 'fup': 0, 'lateralization': 3,
                            'age': 30, 'duration': erd_max_diff, 'psd': psds}

                # np.savez(os.path.join(stats_out_path, out_prefix), ch_names = saved_channels,
                #          num_ec_epochs=len(eeg_epochs), pib=pib, con=con)
                savemat(os.path.join(stats_out_path, out_prefix + '.mat'), save_dic)


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()