'''
Python script to load and ingest EEG records as tensorflow examples.

Mini example:
Full example:
python3 -m ingestion.mayo_ingestion_process_pool \
--data_dir='/home/yoga/data/mef_data/test_mef_corpus/' \
--outfile_name='/home/yoga/data/mef_data/tfrecords/mayo_100_4_20' \
--input_csv='/home/yoga/data/mef_data/downloadable_list.csv' \
--epoch_len=30.0 \
--num_processes=20 \
--num_shards=20


Full example:
python3 -m ingestion.mayo_ingestion_process_pool \
--data_dir='/mnt/Hydrogen/yoga/mef_data/' \
--outfile_name='/Volumes/eplab/Projects/Yoga_EEG_AI/tfrecords/mayo_6276_5_8' \
--input_csv='/mnt/Hydrogen/yoga/downloadable_list.csv' \
--epoch_len=10.0 \
--num_processes=48 \
--num_shards=240
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import glob
import numpy as np
import pandas as pd
from datetime import datetime
import os
import re
import sys
import mne
import tensorflow as tf
from tensorflow.io import gfile
from tensorflow.compat.v1 import flags
from concurrent.futures import ProcessPoolExecutor
from preprocessing.process_eeg import standardize_eeg
from feature_extraction.eeg_stats import eeg_power_band, generate_topomap_data
from utils import eeg_tfexample
from utils.handy_functions import list_contains_substr
from labeling.sleep_staging import stage_yasa

FLAGS = flags.FLAGS

flags.DEFINE_string('data_dir', '', 'Absolute path to the directory where images are located')
flags.DEFINE_string('outfile_name', 'test_tfrecords', 'Output file name including absolute path')
flags.DEFINE_string('input_csv', '', 'Absolute path to the CSV file containing labels')
flags.DEFINE_float('epoch_len', 30.0, 'Length of EEG epochs.')
flags.DEFINE_integer('num_shards', 2, 'Number of shards in TFRecord files.')
flags.DEFINE_integer('num_processes', 2, 'Number of processes to preprocess the records.')

# EEG channels being used
_eeg_channels = ['fp1', 'f3', 'f7', 'c3', 't7', 'p3', 'p7', 'o1',
                 'fp2', 'f4', 'f8', 'c4', 't8', 'p4', 'p8', 'o2',
                 'fz', 'cz', 'pz']


def _process_eeg_files_batch(process_index, ranges, outfile_name, record_names, input_csv, num_shards):
    """Processes and saves list of EEGs as TFRecord in 1 thread.
    Args:
    thread_index: integer, unique batch to run index is within [0, len(ranges)).
    ranges: list of pairs of integers specifying ranges of each batches to
      analyze in parallel.
    outfile_name: string, unique identifier specifying the output file name
    record_names: list of strings; each string is a path to an EEG file
    input_csv: name of the CSV file containing label information.
    num_shards: integer number of shards for this data set.
    """
    # Each thread produces N shards where N = int(num_shards / num_processes).
    # For instance, if num_shards = 128, and the num_processes = 2, then the first
    # thread would produce shards [0, 64).
    num_processes = len(ranges)
    assert not num_shards % num_processes
    num_shards_per_batch = int(num_shards / num_processes)

    shard_ranges = np.linspace(ranges[process_index][0],
                               ranges[process_index][1],
                               num_shards_per_batch + 1).astype(int)
    num_files_in_thread = ranges[process_index][1] - ranges[process_index][0]

    counter = 0

    if not (gfile.exists(input_csv)):
        return

    label_csv = pd.read_csv(gfile.GFile(input_csv))
    unique_csv = label_csv.drop_duplicates(subset="Directory", keep="last")

    for s in range(num_shards_per_batch):
        # Generate a sharded version of the file name, e.g. 'train-00002-of-00010'
        shard = process_index * num_shards_per_batch + s
        output_filename = '%s-%.5d-of-%.5d' % (outfile_name, shard, num_shards)
        writer = tf.io.TFRecordWriter(output_filename)

        shard_counter = 0
        files_in_shard = np.arange(shard_ranges[s], shard_ranges[s + 1], dtype=int)
        for i in files_in_shard:
            record_name = record_names[i]

            # Find patient id and EEG date from EEG name.
            curr_file = os.path.basename(record_name)
            parts = curr_file.split('_')
            mrn = parts[0]
            eeg_date = parts[1][:-5]
            eeg_dt = datetime.strptime(eeg_date, '%Y%m%dT%H%M%S')

            curr_eeg_meta_data = unique_csv[
                (pd.to_datetime(unique_csv["Date"]).dt.date == eeg_dt.date()) & (unique_csv["MRN"] == int(mrn))]

            if len(curr_eeg_meta_data) > 0:
                pathological = curr_eeg_meta_data.Label.values[0]
                if curr_eeg_meta_data.Label.values[0] == 10:
                    print('PNES example: %s,' % record_name)
                    pathological = 5
                age = (eeg_dt.year - pd.to_datetime(curr_eeg_meta_data.MFDOB).dt.year).values[0]

                try:
                    good_epochs, event_list = standardize_eeg(record_name, avg_ref=False, remove_artifacts=False,
                                                          epoch_len=FLAGS.epoch_len)
                except:
                    print('Exception while processing %s.' % record_name)
                    continue

                if good_epochs:

                    eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.rstrip('-').lower() in _eeg_channels]

                    if len(eeg_ch_names) != 19:
                        print('Missing channels in %s.' % record_name)
                        continue

                    try:
                        yasa_sleep_stages = stage_yasa(good_epochs)
                    except:
                        print('Error scoring sleep for %s.' % record_name)
                        continue

                    eeg_epochs = good_epochs.copy().reorder_channels(ch_names=_eeg_channels)

                    new_names = dict(
                        (ch_name,
                         ch_name.rstrip('.').upper().replace('Z', 'z').replace('FP', 'Fp'))
                        for ch_name in eeg_epochs.ch_names)
                    eeg_epochs.rename_channels(new_names)

                    # Set the EEG electrode locations
                    montage = mne.channels.make_standard_montage('standard_1020')
                    eeg_epochs.set_montage(montage)

                    mne.set_eeg_reference(eeg_epochs, ref_channels='average')

                    tot_power, pib = eeg_power_band(eeg_epochs)
                    pib = np.swapaxes(pib, 0, 1)

                    eeg_epochs_resampled = eeg_epochs.resample(sfreq=80)

                    for n, epoch in enumerate(eeg_epochs_resampled):
                        behavioral_state = -1
                        if list_contains_substr(event_list[n], 'eyes open'):
                            behavioral_state = 0
                        elif 'eo' in event_list[n]:
                            behavioral_state = 0
                        elif list_contains_substr(event_list[n], 'eyes closed'):
                            behavioral_state = 1
                        elif 'ec' in event_list[n]:
                            behavioral_state = 1
                        elif list_contains_substr(event_list[n], 'drowsy'):
                            behavioral_state = 2
                        elif list_contains_substr(event_list[n], 'asleep'):
                            behavioral_state = 3
                        elif list_contains_substr(event_list[n], 'snore'):
                            behavioral_state = 3
                        elif list_contains_substr(event_list[n], 'spindles'):
                            behavioral_state = 3
                        elif list_contains_substr(event_list[n], 'spont arousal'):
                            behavioral_state = 4

                        topo_data = generate_topomap_data(pib[n, :, :], eeg_epochs.info)

                        example = eeg_tfexample.create_eeg_example(
                            subject_id=mrn,
                            age=age,
                            eeg_guid=curr_file[:-5],
                            nchannels=epoch.shape[0],
                            nsamples=epoch.shape[1],
                            chan_names=_eeg_channels,
                            chan_types=['eeg'] * len(_eeg_channels),
                            vectorized_data=epoch.reshape(-1),
                            psd_features=pib[n, :, :].reshape(-1),
                            topomap_data=topo_data.reshape(-1),
                            behavioral_state=behavioral_state,
                            behavioral_state_yasa=yasa_sleep_stages[n],
                            pathological=pathological
                        )

                        writer.write(example.SerializeToString())
                    shard_counter += 1
                    counter += 1

                else:
                    print('Could not find good epochs in %s' % record_name)

            else:
                print('Could not locate metadata for %s' % record_name)

            if not counter % 10:
                print('%s [process %d]: Processed %d of %d records in thread batch.' %
                      (datetime.now(), process_index, counter, num_files_in_thread))
            sys.stdout.flush()

        writer.close()
        print('%s [process %d]: Wrote %d records to %s' %
              (datetime.now(), process_index, shard_counter, output_filename))
        sys.stdout.flush()

    print('%s [process %d]: Wrote %d records to %d shards.' %
          (datetime.now(), process_index, counter, num_files_in_thread))
    sys.stdout.flush()

    # return 'Success'


def _process_eeg_files(outfile_name, record_names, input_csv, num_processes, num_shards):
    """Process and save list of EEG records as TFRecord of Example protos.
    Args:
    outfile_name: string, unique identifier specifying the output file name
    record_names: list of strings; each string is a path to an EEG record
    input_csv: name of the CSV file containing label information.
    num_processes: integer number of processes to preprocess the EEGs.
    num_shards: integer number of shards for this data set.
    """

    # Break all images into batches with a [ranges[i][0], ranges[i][1]].
    spacing = np.linspace(0, len(record_names), num_processes + 1).astype(np.int)
    ranges = []
    for i in range(len(spacing) - 1):
        ranges.append([spacing[i], spacing[i+1]])

    # Launch a thread for each batch.
    print('Launching %d processes for spacings: %s' % (num_processes, ranges))
    sys.stdout.flush()

    with ProcessPoolExecutor(max_workers=num_processes) as executor:
        for process_index in range(len(ranges)):
            # args = (process_index, ranges, outfile_name, record_names, input_csv, num_shards)
            executor.submit(_process_eeg_files_batch, process_index, ranges, outfile_name, record_names,
                                     input_csv, num_shards)
            # try:
            #     exit_status = future.result()
            # except Exception as exc:
            #     print(repr(future.exception()))
            # else:
            #     print(f"{exit_status}: {process_index}")

    print('%s: Finished writing all %d records in data set.' %
          (datetime.now(), len(record_names)))
    sys.stdout.flush()


def main(unused_argv):

    all_files = glob.glob(os.path.join(FLAGS.data_dir, '*.mefd'))
    _process_eeg_files(FLAGS.outfile_name, all_files, FLAGS.input_csv, FLAGS.num_processes, FLAGS.num_shards)


if __name__ == '__main__':
    tf.compat.v1.app.run()
