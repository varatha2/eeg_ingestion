'''
Test pipeline.
'''

from __future__ import print_function

import pandas as pd
from glob import glob
import re
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *
import mne
from nilearn import plotting
from matplotlib import pyplot as plt
from feature_extraction.source_localization import SourceLocalization


raw_path = '/Volumes/eplab/Projects/tauPET_epilepsy/lemon/eeglab/'
stats_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/lemon/stats/'
image_out_path = '/Volumes/eplab/Projects/tauPET_epilepsy/lemon/pdf_images/'

localization_method = 'dSPM'
epoch_len = 10.
_eeg_channels = ['fp1', 'f3', 'f7', 'f9', 'c3', 't7', 't9', 'tp11', 'p3', 'p7', 'p9', 'o1',
                 'fp2', 'f4', 'f8', 'f10', 'c4', 't8', 't10', 'tp12', 'tp9', 'tp10', 'p4', 'p8', 'p10', 'o2',
                 'fpz', 'fz', 'cz', 'pz', 'oz']
_freq_bands = {"delta": [2, 4],
               "theta": [4, 8],
               "alpha1": [8, 10],
               "alpha2": [10, 13],
               "beta": [13, 25],
               "gamma": [25, 40]}

def is_eeg_channel(ch):
    '''
    Determine if channel name is EEG channel.
    :param ch: string channel name
    :return: True if channel name is EEG channel
    '''

    # only exception is O2SAT because it can be confused with O2
    if 'o2' in ch.lower() and 'sat' in ch.lower():
        return False

    # use EEG channel names in the standard 10-20 montage
    std_montage = mne.channels.make_standard_montage('standard_1020')
    _all_eeg_channels = [_ch.lower() for _ch in std_montage.ch_names]

    for _ch in _all_eeg_channels:
        if _ch in ch.lower():
            return True

    return False


def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)

def main():
    """Test module functions."""

    os.system("clear")

    all_eegs = sorted(glob(os.path.join(raw_path, '*_EC.set')))

    # initialize source localization
    sl = SourceLocalization(freq_bands=_freq_bands, epoch_len=epoch_len)
    # sl = SourceLocalization(freq_bands=_freq_bands, epoch_len=epoch_len,
    #                         vol_labels=['Left-Cerebral-Cortex', 'Right-Cerebral-Cortex'])

    # process files
    for curr_eeg in all_eegs:
        record_name = os.path.basename(curr_eeg)
        out_prefix = re.sub(r'\W+', '', record_name)

        try:
            ec_epochs, event_list = standardize_eeg(curr_eeg, avg_ref=False, epoch_len=10,
                                                    epoch_rule='fixed', reject_bad=True, remove_artifacts=False)
        except Exception as e:
            print(e)
            print('Exception while processing %s.' % record_name)
            continue

        if ec_epochs:
            print(record_name)
            print(ec_epochs.info)

            eeg_ch_names = [ch for ch in ec_epochs.ch_names if is_eeg_channel(ch.split('-')[0].lower())]

            print('# channels: ' + str(len(eeg_ch_names)))

            eeg_epochs = ec_epochs.copy().pick(picks=eeg_ch_names)  # pick only EEG data

            new_names = dict(
                (ch_name,
                 ch_name.rstrip('.').upper().replace('Z', 'z').replace('FP', 'Fp').replace('TP11', 'TP9').replace(
                     'TP12', 'TP10'))
                for ch_name in eeg_epochs.ch_names)
            eeg_epochs.rename_channels(new_names)

            # Read and set the EEG electrode locations
            montage = mne.channels.make_standard_montage('standard_1020')
            # montage.ch_names
            eeg_epochs.set_montage(montage)
            eeg_epochs.set_eeg_reference(projection=True)  # needed for inverse modeling

            print("EC epochs found: " + str(len(eeg_epochs)))

            eeg_epochs = eeg_epochs[:min(len(eeg_epochs), 10)]

            if len(eeg_epochs) > 0:

                stcs = sl.compute_source_estimate_epochs(eeg_epochs, method=localization_method)

                # save STCs as CSV
                stc_dfs = []
                for band in stcs:
                    stc_dfs.append(stcs[band].to_data_frame())

                index_mapping = {i: k for i, (k, v) in enumerate(_freq_bands.items())}
                stc_df = pd.concat(stc_dfs, ignore_index=True).rename(index=index_mapping)
                stc_df.to_csv(os.path.join(stats_out_path, out_prefix + '_stc_psd_%s.csv' % localization_method))

                print('Saving source images..')

                fig, axs = plt.subplots(1, len(stcs.keys()), figsize=(18, 2))
                for i, key in enumerate(stcs.keys()):
                    nii_name = os.path.join(stats_out_path, out_prefix + '_%s_%s.nii.gz' % (key, localization_method))
                    sl.export_nifti_image(stcs[key], nii_name)
                    plotting.plot_img(nii_name, figure=fig, display_mode='z', cut_coords=[0],
                                              axes=axs[i], colorbar=True, annotate=False, draw_cross=False, vmin=0,
                                              vmax=30, cmap='jet')
                    # plotting.plot_glass_brain(nii_name, figure=fig, display_mode='z', cut_coords=[0], vmin=0, vmax=30,
                    #                           axes=axs[i], colorbar=True, annotate=False, draw_cross=False, cmap='jet')
                    axs[i].set_title(key)
                plt.savefig(os.path.join(image_out_path, out_prefix + '_%s.png' % localization_method), dpi=300.)

                # apply avg reference (computed earlier)
                eeg_epochs.apply_proj()

                tot_power, pib = eeg_power_band(eeg_epochs)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                print(pib.shape)

                con = calc_connectivity(eeg_epochs, method=['coh', 'imcoh', 'pli'], average=False)
                print(con.shape)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                all_ec_epochs = mne.concatenate_epochs([eeg_epochs[i] for i in range(len(eeg_epochs))], add_offset=False)
                split_data = np.vsplit(all_ec_epochs.get_data(), len(all_ec_epochs))
                full_data = np.squeeze(np.dstack(split_data), axis=0)
                gfp, state_maps, gfp_per_state, p_hat, T_hat, gev, gev_sum = calc_microstate_stats(full_data,
                                                                                                   eeg_epochs.info[
                                                                                                       'sfreq'],
                                                                                                   eeg_epochs.info[
                                                                                                       'ch_names'])

                np.savez(os.path.join(stats_out_path, out_prefix), ch_names=eeg_epochs.ch_names,
                         num_ec_epochs=len(eeg_epochs), pib=pib, coh=coh, imcoh=imcoh, pli=pli, gfp=gfp,
                         ms_maps=state_maps, gfp_per_state=gfp_per_state, p_hat=p_hat, t_hat=T_hat,
                         gev=gev, gev_sum=gev_sum)

                # ec_epochs.save(os.path.join(epochs_out_path, out_prefix + '-EC-epo.fif'), overwrite=True)

        sl.reset()

        # delete mef
        # shutil.rmtree(mef_out_name + '.mefd/')


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()