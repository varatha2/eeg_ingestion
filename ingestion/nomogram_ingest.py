from __future__ import print_function

from glob import glob
import re
from scipy.io import savemat

from preprocessing.process_eeg import *
from feature_extraction.eeg_stats import *

_eeg_channels = ['fp1', 'f3', 'f7', 'f9', 'c3', 't7', 't9', 'tp11', 'p3', 'p7', 'p9', 'o1',
                 'fp2', 'f4', 'f8', 'f10', 'c4', 't8', 't10', 'tp12', 'tp9', 'tp10', 'p4', 'p8', 'p10', 'o2',
                 'fpz', 'fz', 'cz', 'pz', 'oz']


# input_path = '/Volumes/m145916/Projects/CCF_Nomogram/EEG/PNES_EC/'
# stats_out_path = '/Volumes/m145916/Projects/CCF_Nomogram/pipeline_stats/pnes/'
input_path = '/Volumes/m145916/Projects/CCF_Nomogram/EEG/ec_lay/'
stats_out_path = '/Volumes/m145916/Projects/CCF_Nomogram/pipeline_stats/ccf/'
# input_path = '/Volumes/m145916/Projects/CCF_Nomogram/EEG/Mayo/ec_lay/'
# stats_out_path = '/Volumes/m145916/Projects/CCF_Nomogram/pipeline_stats/mayo/'

def list_contains_substr(curr_list, substr):
    '''

    :param curr_list: list containing strings
    :param substr: sub string to be checked for presence in curr_list
    :return: True or False
    '''

    return any(substr in s for s in curr_list)

def main():
    """Test module functions."""

    os.system("clear")

    all_files = glob(os.path.join(input_path, '*.lay')) + glob(os.path.join(input_path, '*.edf'))

    # process files
    for curr_file in all_files:
        record_name = os.path.basename(curr_file)
        out_prefix = re.sub(r'\W+', '', record_name)

        curr_file = curr_file.replace(" ", "\\ ")

        good_epochs, event_list = standardize_eeg(curr_file, avg_ref=False, reject_bad=False, remove_artifacts=False)
        eeg_ch_names = [ch for ch in good_epochs.ch_names if ch.split('-')[0].lower() in _eeg_channels]
        saved_channels = [ch.split('-')[0].lower() for ch in eeg_ch_names]
        # eeg_indx = [ch_names.index(ch) for ch in ch_names if ch in _eeg_channels]
        if good_epochs:
            print(good_epochs.info)
            print(len(event_list))

            eeg_epochs = good_epochs.pick(picks=eeg_ch_names) # pick only EEG data

            if len(eeg_epochs) > 0:

                # average reference
                mne.set_eeg_reference(eeg_epochs, ref_channels='average', copy=False, projection=False)

                tot_power, pib = eeg_power_band(eeg_epochs)
                pib = np.swapaxes(pib, 0, 1)  # epochs are in axis 1 --> move to axis 0
                print(pib.shape)

                con = calc_connectivity(eeg_epochs, method=['coh', 'imcoh', 'pli'], average=False)
                print(con.shape)

                coh = con[:, 0, :, :, :]
                imcoh = con[:, 1, :, :, :]
                pli = con[:, 2, :, :, :]

                save_dic = {"channels": saved_channels, "tot_power": tot_power, "pib": pib, "coh": coh,
                            "imcoh": imcoh, "pli": pli}

                # np.savez(os.path.join(stats_out_path, out_prefix), ch_names = saved_channels,
                #          num_ec_epochs=len(eeg_epochs), pib=pib, con=con)
                savemat(os.path.join(stats_out_path, out_prefix.replace('1539', '1593') + '.mat'), save_dic)


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    main()